using FluentAssertions;
using IndecScraper.Values;
using Xunit;

namespace IndecScraper.Tests.Values;

public class MonthsPeriodTests
{
    public static DateTime FromYearMonth(int year, int month)
        => new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);

    public static IEnumerable<object[]> Parse_BuildsCorrectValue_TestData = new List<object[]>
    {
        new object[] { "2020-01", "2020-12", new MonthsPeriod(FromYearMonth(2020, 1), FromYearMonth(2020, 12)) },
        new object[] { "0015-01", "3000-06", new MonthsPeriod(FromYearMonth(15, 1), FromYearMonth(3000, 6)) },
        new object[] { "2019-03", "2020-06", new MonthsPeriod(FromYearMonth(2019, 3), FromYearMonth(2020, 6)) },
    };
    [Theory]
    [MemberData(nameof(Parse_BuildsCorrectValue_TestData))]
    public void Parse_BuildsCorrectValue(string from, string to, MonthsPeriod expectedValue)
    {
        var result = MonthsPeriod.Parse(from, to);
        result.Should().Be(expectedValue);
    }

    [Theory]
    [InlineData("20-01", "2020-02")]
    [InlineData("abcd-12", "2020-02")]
    [InlineData("2022-01", "2023-2")]
    [InlineData("2020-04", "2021")]
    [InlineData("2020-", "2021-01")]
    public void Parse_ThrowsFormatExceptionWhenStringCannotBeParsed(string from, string to)
    {
        var action = () => MonthsPeriod.Parse(from, to);
        action.Should().Throw<FormatException>();
    }
}