using FluentAssertions;
using IndecScraper.Values;
using Xunit;

namespace IndecScraper.Tests.Values;

public class PercentageTests
{
    [Theory]
    [InlineData(15, 15, true)]
    [InlineData(0, 15, false)]
    [InlineData(90, 90.1, false)]
    [InlineData(0.2, 0.25, false)]
    public void Equals_ComparesByUnderlyingValue(decimal x, decimal y, bool expected)
    {
        var xp = new Percentage(x);
        var yp = new Percentage(y);
        xp.Equals(yp).Should().Be(expected);
    }

    [Theory]
    [InlineData(21, 15, 39.15)]
    [InlineData(1, 1, 2.01)]
    [InlineData(10, 10, 21)]
    public void AddTo_YieldsTheExpectedResult(decimal x, decimal y, decimal expected)
    {
        var xp = new Percentage(x);
        var yp = new Percentage(y);
        xp.AddTo(yp).Value.Should().BeApproximately(expected, 0.01m);
    }
}
