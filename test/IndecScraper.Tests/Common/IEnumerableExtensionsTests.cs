using FluentAssertions;
using IndecScraper.Common;
using Xunit;

namespace IndecScraper.Tests.Common;

public class IEnumerableExtensionsTests
{
    [Theory]
    [InlineData(0, 10, 0)]
    [InlineData(0, 0, 0)]
    [InlineData(10, 0, 0)]
    [InlineData(1, 10, 1)]
    [InlineData(10, 10, 1)]
    [InlineData(100, 10, 10)]
    [InlineData(120, 12, 10)]
    [InlineData(90, 7, 13)]
    [InlineData(15, 1, 15)]
    [InlineData(21, 2, 11)]
    public void Partition_ReturnsCorrectValues(int enumCount, int itemsByPart, int expectedCount)
    {
        var input = Enumerable.Range(0, enumCount);
        var result = input.Partition(itemsByPart);
        result.Should().HaveCount(expectedCount);

        for (int i = 0; i < expectedCount; i++)
        {
            if (i < expectedCount - 1)
                result.ElementAt(i).Should().HaveCount(itemsByPart);
            else
                result.ElementAt(i).Should().HaveCountLessThanOrEqualTo(itemsByPart);
            result.ElementAt(i).Should()
                .OnlyContain(x => x >= itemsByPart * i)
                .And.OnlyContain(x => x < itemsByPart * (i + 1));
        }
    }
}
