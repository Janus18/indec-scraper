namespace IndecScraper.Tests.TestUtils;

public static class DateTimeUtils
{
    public static DateTime FromYearMonth(int year, int month)
        => FromYearMonthDay(year, month, 1);

    public static DateTime FromYearMonthDay(int year, int month, int day)
        => new DateTime(year, month, day, 0, 0, 0, DateTimeKind.Utc);
}
