using AutoFixture;
using IndecScraper.Values;

namespace IndecScraper.Tests.TestUtils.Mocks;

public static class MonthCPI
{
    public static IndecScraper.Models.MonthCPI Get(
        Fixture fixture,
        DateTime? period = null,
        Percentage? index = null
    ) => new IndecScraper.Models.MonthCPI(
        fixture.Create<DateTime>(),
        index ?? new Percentage(fixture.Create<decimal>()  % 100),
        period ?? fixture.Create<DateTime>()
    );
}