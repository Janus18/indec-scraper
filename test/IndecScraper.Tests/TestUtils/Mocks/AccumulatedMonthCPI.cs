using AutoFixture;

namespace IndecScraper.Tests.TestUtils.Mocks;

public static class AccumulatedMonthCPI
{
    public static IndecScraper.Values.AccumulatedMonthCPI Get(Fixture fixture)
        => new IndecScraper.Values.AccumulatedMonthCPI(
            MonthCPI.Get(fixture),
            new IndecScraper.Values.Percentage(fixture.Create<decimal>() % 100)
        );
}