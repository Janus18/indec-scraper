using AutoFixture;
using FluentAssertions;
using IndecScraper.Application;
using IndecScraper.Application.Commands;
using IndecScraper.Application.Commands.Contract;
using Xunit;

namespace IndecScraper.Tests.Application;

public class CommandTests
{
    private static readonly Fixture Fixture = new();
    
    [Theory]
    [InlineData(nameof(Checkpoint), typeof(Checkpoint))]
    [InlineData(nameof(Configuration), typeof(Configuration))]
    [InlineData(nameof(Download), typeof(Download))]
    public void GetCommand_ForParameterlessCommands_CreatesCorrectCommand(string name, Type expectedType)
    {
        var command = Command.GetCommand(new[] { name });
        command.Should().NotBeNull()
            .And.BeOfType(expectedType);
    }

    [Theory]
    [InlineData(nameof(Bootstrap), typeof(Bootstrap))]
    [InlineData(nameof(Sync), typeof(Sync))]
    public void GetCommand_CreatesSyncAndBootstrapCommands_WithCorrectValues(string name, Type expectedType)
    {
        var password = Fixture.Create<string>();
        var command = Command.GetCommand(new[] { name, password });
        command.Should().NotBeNull()
            .And.BeOfType(expectedType)
            .And.BeAssignableTo<IProtectedCommand>()
            .Which.Password.Should().Be(password);
    }

    
    [Fact]
    public void GetCommand_CreatesRateCommandWithCorrectValues()
    {
        var date1 = Fixture.Create<DateTime>();
        var date2 = Fixture.Create<DateTime>();
        var command = Command.GetCommand(new[] { nameof(Rate), date1.ToString("yyyy-MM"), date2.ToString("yyyy-MM") });
        var rateCommand = command.Should().NotBeNull().And.BeOfType<Rate>().Which;
        rateCommand.Period.from.Should().HaveYear(date1.Year).And.HaveMonth(date1.Month);
        rateCommand.Period.to.Should().HaveYear(date2.Year).And.HaveMonth(date2.Month);
    }
    
    [Fact]
    public void GetCommand_CreatesValuesCommandWithCorrectValues()
    {
        var date1 = Fixture.Create<DateTime>();
        var date2 = Fixture.Create<DateTime>();
        var command = Command.GetCommand(new[]
        {
            nameof(IndecScraper.Application.Commands.Values),
            date1.ToString("yyyy-MM"),
            date2.ToString("yyyy-MM")
        });
        var valuesCommand = command.Should().NotBeNull().And.BeOfType<IndecScraper.Application.Commands.Values>().Which;
        valuesCommand.Period.from.Should().HaveYear(date1.Year).And.HaveMonth(date1.Month);
        valuesCommand.Period.to.Should().HaveYear(date2.Year).And.HaveMonth(date2.Month);
    }
}