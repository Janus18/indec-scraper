using System.Linq.Expressions;
using AutoFixture;
using FluentAssertions;
using IndecScraper.Application.Commands.Handlers;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Values;
using Moq;
using Serilog;
using Xunit;

namespace IndecScraper.Tests.Application.Commands.Handlers;

public class ValuesCommandHandlerTests : IAsyncLifetime
{
    private static readonly Fixture fixture = new();
    private static readonly Mock<IDatabaseService> databaseService = new();
    private static readonly Mock<ILogger> logger = new();
    private static readonly ValuesCommandHandler handler = new(databaseService.Object, logger.Object);

    public Task DisposeAsync()
    {
        databaseService.Reset();
        logger.Reset();
        return Task.CompletedTask;
    }

    public Task InitializeAsync() => Task.CompletedTask;

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(20)]
    public async Task FindRange_ReturnsList_Of_MonthCPI(int monthsCount)
    {
        var monthsCpis = Enumerable.Range(0, monthsCount)
            .Select(_ => Tests.TestUtils.Mocks.MonthCPI.Get(fixture))
            .ToList();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), null))
            .ReturnsAsync(monthsCpis)
            .Verifiable();

        var result = await handler.Handle(new IndecScraper.Application.Commands.Values("2020-01", "2020-12"), default);

        result.Should().NotBeNull().And.HaveCount(monthsCount);
        if (monthsCount > 0)
        {
            result.Should().OnlyContain(x => monthsCpis.Any(y => x.Period == y.Period && x.Index == y.Index));
        }

        databaseService.Verify();
    }

    [Theory]
    [InlineData("2020-01", "2022-12", 36)]
    [InlineData("2020-01", "2020-12", 12)]
    [InlineData("2020-01", "2020-02", 2)]
    [InlineData("2020-01", "2020-01", 1)]
    [InlineData("2020-01", "2019-12", 0)]
    [InlineData("2022-06", "2022-12", 7)]
    public async Task FindRange_FiltersCPIsByDates_ReturnsList_Of_MonthCPI(string from, string to, int expectedCount)
    {
        var initialDate = new DateTime(2020, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        var monthsCpis = Enumerable.Range(0, 36)
            .Select(i => Tests.TestUtils.Mocks.MonthCPI.Get(fixture, initialDate.AddMonths(i)))
            .ToList();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), null))
            .Returns<Expression<Func<MonthCPI, bool>>, ITransaction?>(
                (exp, t) => Task.FromResult(monthsCpis.Where(x => exp.Compile().Invoke(x)))
            );

        var period = MonthsPeriod.Parse(from, to);
        var result = await handler.Handle(IndecScraper.Application.Commands.Values.Make(period), default);

        result.Should().NotBeNull().And.HaveCount(expectedCount);
        if (expectedCount > 0)
        {
            result.Should().OnlyContain(x => x.Period >= period.from && x.Period <= period.to);
        }
    }
}
