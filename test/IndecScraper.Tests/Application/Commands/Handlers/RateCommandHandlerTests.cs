using AutoFixture;
using FluentAssertions;
using IndecScraper.Application.Commands.Handlers;
using IndecScraper.Values;
using MediatR;
using Moq;
using Xunit;

namespace IndecScraper.Tests.Application.Commands.Handlers;

public class RateCommandHandlerTests : IAsyncLifetime
{
    private static readonly Fixture fixture = new();
    private static readonly Mock<IRequestHandler<IndecScraper.Application.Commands.Values, IEnumerable<AccumulatedMonthCPI>>> valuesHandler = new();
    private static readonly RateCommandHandler handler = new(valuesHandler.Object);


    public Task DisposeAsync()
    {
        valuesHandler.Reset();
        return Task.CompletedTask;
    }

    public Task InitializeAsync() => Task.CompletedTask;

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(15)]
    [InlineData(30)]
    public async Task CalculateRangeInflationRate_ReturnsCorrectIndex(int monthCPIsCount)
    {
        var monthsCpis = Enumerable.Range(0, monthCPIsCount)
            .Select(_ => IndecScraper.Tests.TestUtils.Mocks.AccumulatedMonthCPI.Get(fixture))
            .ToList();

        var period = MonthsPeriod.Parse("2020-01", "2020-12");

        valuesHandler.Setup(
            s => s.Handle(It.Is<IndecScraper.Application.Commands.Values>(v => v.Period == period), It.IsAny<CancellationToken>())
        ).ReturnsAsync(monthsCpis).Verifiable();

        var result = await handler.Handle(new IndecScraper.Application.Commands.Rate("2020-01", "2020-12"), default);
        result.Should().Be(monthsCpis.LastOrDefault()?.Accumulated ?? new Percentage(0m));

        valuesHandler.Verify();
    }
}
