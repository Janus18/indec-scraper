using AutoFixture;
using FluentAssertions;
using IndecScraper.Application.Commands.Handlers;
using IndecScraper.Application.Services;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Models.Interfaces;
using Moq;
using Xunit;

namespace IndecScraper.Tests.Application.Commands.Handlers;

public class SyncCommandHandlerTests : IAsyncLifetime
{
    private static readonly Fixture fixture = new();
    private static readonly Mock<IWebReader> webReader = new();
    private static readonly Mock<ICPISynchronization> cpiSynchronization = new();
    private static readonly Mock<IDatabaseService> databaseService = new();
    private static readonly Mock<ISyncScheduler> syncScheduler = new();
    private static readonly SyncCommandHandler handler = new(
        webReader.Object,
        cpiSynchronization.Object,
        databaseService.Object,
        syncScheduler.Object
    );

    public Task DisposeAsync()
    {
        webReader.Reset();
        cpiSynchronization.Reset();
        databaseService.Reset();
        syncScheduler.Reset();
        return Task.CompletedTask;
    }

    public Task InitializeAsync() => Task.CompletedTask;

    [Fact]
    public async Task Handle_ReadsWeb_AndSyncsValues_ReturnsWebsiteCheckpoint_CallsSyncScheduler()
    {
        var monthsCpis = Enumerable.Range(0, 15)
            .Select(_ => Tests.TestUtils.Mocks.MonthCPI.Get(fixture))
            .ToList();

        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(monthsCpis);

        webReader.Setup(r => r.ReadWebsite())
            .ReturnsAsync(website.Object)
            .Verifiable();

        var transaction = new Mock<ITransaction>();
        databaseService.Setup(s => s.CreateTransaction())
            .ReturnsAsync(transaction.Object)
            .Verifiable();

        var websiteCheckpoint = new WebsiteCheckpoint(true);
        cpiSynchronization.Setup(s => s.SyncWebsite(website.Object, transaction.Object))
            .ReturnsAsync(websiteCheckpoint)
            .Verifiable();

        var result = await handler.Handle(new IndecScraper.Application.Commands.Sync(fixture.Create<string>()), default);
        result.Should().NotBeNull();
        result.Should().Be(websiteCheckpoint);

        syncScheduler.Verify(s => s.ScheduleSync(websiteCheckpoint), Times.Once);

        webReader.Verify();
        databaseService.Verify();
        cpiSynchronization.Verify();

        transaction.Verify(t => t.Commit(), Times.Once);
    }
}
