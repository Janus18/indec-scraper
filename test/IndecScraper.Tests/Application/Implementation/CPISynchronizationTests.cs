using System.Linq.Expressions;
using AutoFixture;
using FluentAssertions;
using IndecScraper.Application.Services;
using IndecScraper.Application.Services.Implementation;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Models.Interfaces;
using IndecScraper.Values;
using Moq;
using Serilog;
using Xunit;

namespace IndecScraper.Tests.Application.Implementation;

public class CPISynchronizationTests : IAsyncLifetime
{
    private static readonly Fixture fixture = new();
    private static readonly Mock<IDatabaseService> databaseService = new();
    private static readonly Mock<ILogger> logger = new();
    private static readonly ICPISynchronization cpiSynchronization =
        new CPISynchronization(databaseService.Object, logger.Object);

    public Task DisposeAsync()
    {
        databaseService.Reset();
        return Task.CompletedTask;
    }

    public Task InitializeAsync() => Task.CompletedTask;

    [Fact]
    public async Task SyncWebsite_ReturnsWebsiteCheckpoint()
    {
        var transaction = new Mock<ITransaction>();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), transaction.Object))
            .ReturnsAsync(new List<MonthCPI>())
            .Verifiable();

        var checksum = fixture.Create<string>();

        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(Enumerable.Range(0, 10).Select(_ => TestUtils.Mocks.MonthCPI.Get(fixture)).ToList());

        var result = await cpiSynchronization.SyncWebsite(website.Object, transaction.Object);
        
        result.Should().NotBeNull();
        result.Created.Should().Be(10);
        result.Timestamp.Should().BeCloseTo(DateTime.UtcNow, new TimeSpan(0, 0, 5));
        result.Updated.Should().Be(0);
    }

    [Fact]
    public async Task SyncWebsite_WhenWebsiteHasNoCPIEntries_DoesNothing_ReturnsWebsiteCheckpoint()
    {
        var transaction = new Mock<ITransaction>();
        var checksum = fixture.Create<string>();

        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(new List<MonthCPI>());
        
        var result = await cpiSynchronization.SyncWebsite(website.Object, transaction.Object);
        
        result.Should().NotBeNull();
        result.Created.Should().Be(0);
        result.Timestamp.Should().BeCloseTo(DateTime.UtcNow, new TimeSpan(0, 0, 5));
        result.Updated.Should().Be(0);

        databaseService.VerifyNoOtherCalls();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(10)]
    [InlineData(20)]
    [InlineData(100)]
    public async Task SyncWebsite_AddsIndexesNotFound(int valuesCount)
    {
        var transaction = new Mock<ITransaction>();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), transaction.Object))
            .ReturnsAsync(new List<MonthCPI>())
            .Verifiable();

        var values = Enumerable.Range(0, valuesCount)
            .Select(_ => TestUtils.Mocks.MonthCPI.Get(fixture))
            .ToList();

        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(values);

        var result = await cpiSynchronization.SyncWebsite(website.Object, transaction.Object);
        
        result.Should().NotBeNull();
        result.Created.Should().Be(valuesCount);
        result.Updated.Should().Be(0);

        databaseService.Verify();
        foreach (var value in values)
        {
            databaseService.Verify(s => s.Save(It.Is<MonthCPI>(
                x => x.DatePublished == value.DatePublished && x.Index == value.Index && x.Period == value.Period
            ), transaction.Object), Times.Once);
        }
        databaseService.VerifyNoOtherCalls();
    }

    [Theory]
    [InlineData(1)]
    [InlineData(10)]
    [InlineData(20)]
    [InlineData(100)]
    public async Task SyncWebsite_UpdatesValuesFound_WithADifferentIndex(int valuesCount)
    {
        var transaction = new Mock<ITransaction>();

        var values = Enumerable.Range(0, valuesCount)
            .Select(_ => TestUtils.Mocks.MonthCPI.Get(fixture))
            .ToList();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), transaction.Object))
            .ReturnsAsync(values)
            .Verifiable();

        var inputValues = values
            .Select(v => new MonthCPI(v.DatePublished, new Percentage(v.Index.Value + 1), v.Period))
            .ToList();
        
        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(inputValues);

        var result = await cpiSynchronization.SyncWebsite(website.Object, transaction.Object);
        
        result.Should().NotBeNull();
        result.Created.Should().Be(0);
        result.Updated.Should().Be(valuesCount);

        databaseService.Verify();
        foreach (var value in values)
        {
            databaseService.Verify(s => s.Update(value, transaction.Object), Times.Once);
        }
        databaseService.VerifyNoOtherCalls();
    }

    [Fact]
    public async Task SyncWebsite_DoesNotUpdateValuesFoundWithSameIndex()
    {
        var transaction = new Mock<ITransaction>();

        var values = Enumerable.Range(0, 10)
            .Select(_ => TestUtils.Mocks.MonthCPI.Get(fixture))
            .ToList();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), transaction.Object))
            .ReturnsAsync(values)
            .Verifiable();

        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(values);

        var result = await cpiSynchronization.SyncWebsite(website.Object, transaction.Object);
        
        result.Should().NotBeNull();
        result.Created.Should().Be(0);
        result.Updated.Should().Be(0);

        databaseService.Verify();
        databaseService.VerifyNoOtherCalls();
    }

    [Fact]
    public async Task SyncWebsite_SearchesByPeriod()
    {
        var transaction = new Mock<ITransaction>();

        var inputValues = Enumerable.Range(0, 10)
            .Select(_ => TestUtils.Mocks.MonthCPI.Get(fixture))
            .ToList();

        var existingValues = inputValues
            .Select(v => TestUtils.Mocks.MonthCPI.Get(fixture, v.Period, new Percentage(v.Index.Value + 1)))
            .ToList();

        databaseService.Setup(s => s.GetMany(It.IsAny<Expression<Func<MonthCPI, bool>>>(), transaction.Object))
            .Returns<Expression<Func<MonthCPI, bool>>, ITransaction>(
                (exp, t) => Task.FromResult<IEnumerable<MonthCPI>>(existingValues.Where(v => exp.Compile().Invoke(v)))
            ).Verifiable();

        var website = new Mock<IWebsite>();
        website.Setup(w => w.GetCPIEntries())
            .Returns(inputValues);

        var result = await cpiSynchronization.SyncWebsite(website.Object, transaction.Object);
        
        result.Should().NotBeNull();
        result.Created.Should().Be(0);
        result.Updated.Should().Be(10);

        databaseService.Verify();
        foreach (var value in inputValues)
        {
            var existingValue = existingValues.First(v => v.Period == value.Period);
            value.Index.Should().Be(existingValue.Index);
        }
        foreach (var value in existingValues)
        {
            databaseService.Verify(s => s.Update(value, It.IsAny<ITransaction>()), Times.Once);
        }
        databaseService.VerifyNoOtherCalls();
    }
}