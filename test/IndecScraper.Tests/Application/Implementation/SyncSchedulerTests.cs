using System.Net;
using Amazon.Scheduler;
using Amazon.Scheduler.Model;
using AutoFixture;
using IndecScraper.Application.Configuration;
using IndecScraper.Application.Services.Implementation;
using IndecScraper.Models;
using Microsoft.Extensions.Options;
using Moq;
using Serilog;
using Xunit;

namespace IndecScraper.Tests.Application.Implementation;

public class SyncSchedulerTests : IAsyncLifetime
{
    private static readonly Fixture Fixture = new();
    private static readonly Mock<IAmazonScheduler> AmazonScheduler = new();
    private static readonly ApplicationConfiguration Configuration = Fixture.Create<ApplicationConfiguration>();
    private static readonly Mock<ILogger> Logger = new();
    private static readonly SyncScheduler SyncScheduler =
        new(AmazonScheduler.Object, Options.Create(Configuration), Logger.Object);

    public Task DisposeAsync()
    {
        AmazonScheduler.Reset();
        Logger.Reset();
        return Task.CompletedTask;
    }

    public Task InitializeAsync() => Task.CompletedTask;

    [Fact]
    public async Task ScheduleSync_WhenWebsiteCheckpointIsNotNull_CreatesAWSSchedule_WithCorrectData()
    {
        var websiteCheckpoint = new WebsiteCheckpoint();
        
        var createScheduleResponse = Fixture.Create<CreateScheduleResponse>();

        var nextExecution = websiteCheckpoint.NextExecution.ToString("yyyy-MM-ddTHH:mm:ss");

        AmazonScheduler.Setup(s => s.CreateScheduleAsync(It.Is<CreateScheduleRequest>(r =>
            r.Name.StartsWith("indecscraper-schedule-") &&
            r.ScheduleExpression == $"at({nextExecution})" &&
            r.ScheduleExpressionTimezone == "Etc/UTC" &&
            r.State == ScheduleState.ENABLED &&
            r.Target != null &&
            r.Target.Arn == Configuration.LambdaArn &&
            r.Target.RoleArn == Configuration.EventBridgeRoleArn
        ), default)).ReturnsAsync(createScheduleResponse).Verifiable();

        await SyncScheduler.ScheduleSync(websiteCheckpoint);

        AmazonScheduler.Verify();
    }

    [Fact]
    public async Task ScheduleSync_WhenWebsiteCheckpointIsNull_CreatesAWSSchedule_WithCorrectData()
    {
        var createScheduleResponse = Fixture.Create<CreateScheduleResponse>();

        var now = DateTime.UtcNow;
        var possibleTimes = Enumerable.Range(0, 5)
            .Select(x => now.AddSeconds(30).AddSeconds(x))
            .Select(x => x.ToString("yyyy-MM-ddTHH:mm:ss"))
            .ToList();

        AmazonScheduler.Setup(s => s.CreateScheduleAsync(It.Is<CreateScheduleRequest>(r =>
            r.Name.StartsWith("indecscraper-schedule-") &&
            possibleTimes.Any(t => r.ScheduleExpression == $"at({t})") &&
            r.ScheduleExpressionTimezone == "Etc/UTC" &&
            r.State == ScheduleState.ENABLED &&
            r.Target != null &&
            r.Target.Arn == Configuration.LambdaArn &&
            r.Target.RoleArn == Configuration.EventBridgeRoleArn
        ), default)).ReturnsAsync(createScheduleResponse).Verifiable();

        await SyncScheduler.ScheduleSync(null);

        AmazonScheduler.Verify();
    }

    [Theory]
    [InlineData(HttpStatusCode.OK)]
    [InlineData(HttpStatusCode.NoContent)]
    public async Task ScheduleSync_WhenResponseIsOk_LogsInfo(HttpStatusCode responseStatus)
    {
        var websiteCheckpoint = new WebsiteCheckpoint();

        var createScheduleResponse = Fixture.Build<CreateScheduleResponse>()
            .With(r => r.HttpStatusCode, responseStatus)
            .Create();

        AmazonScheduler.Setup(s => s.CreateScheduleAsync(It.IsAny<CreateScheduleRequest>(), default))
            .ReturnsAsync(createScheduleResponse);

        await SyncScheduler.ScheduleSync(websiteCheckpoint);

        Logger.Verify(l => l.Information(It.IsAny<string>(), It.IsAny<It.IsAnyType>(), It.IsAny<It.IsAnyType>()));
    }

    [Theory]
    [InlineData(HttpStatusCode.BadRequest)]
    [InlineData(HttpStatusCode.BadGateway)]
    public async Task ScheduleSync_WhenResponseIsNotOk_LogsError(HttpStatusCode responseStatus)
    {
        var websiteCheckpoint = new WebsiteCheckpoint();

        var createScheduleResponse = Fixture.Build<CreateScheduleResponse>()
            .With(r => r.HttpStatusCode, responseStatus)
            .Create();

        AmazonScheduler.Setup(s => s.CreateScheduleAsync(It.IsAny<CreateScheduleRequest>(), default))
            .ReturnsAsync(createScheduleResponse);

        await SyncScheduler.ScheduleSync(websiteCheckpoint);

        Logger.Verify(l => l.Error(It.IsAny<string>(), It.IsAny<It.IsAnyType>(), It.IsAny<It.IsAnyType>()));
    }

    [Fact]
    public async Task ScheduleSync_DeletesOldSchedules()
    {
        var websiteCheckpoint = new WebsiteCheckpoint();

        var createScheduleResponse = Fixture.Build<CreateScheduleResponse>()
            .With(r => r.HttpStatusCode, HttpStatusCode.OK)
            .Create();
        AmazonScheduler.Setup(s => s.CreateScheduleAsync(It.IsAny<CreateScheduleRequest>(), default))
            .ReturnsAsync(createScheduleResponse);

        var listSchedulesResponse = Fixture.Create<ListSchedulesResponse>();
        AmazonScheduler.Setup(s => s.ListSchedulesAsync(It.IsAny<ListSchedulesRequest>(), default))
            .ReturnsAsync(listSchedulesResponse)
            .Verifiable();

        await SyncScheduler.ScheduleSync(websiteCheckpoint);

        AmazonScheduler.Verify();
        foreach (var schedule in listSchedulesResponse.Schedules)
        {
            AmazonScheduler.Verify(s => s.DeleteScheduleAsync(It.Is<DeleteScheduleRequest>(r => r.Name == schedule.Name), default), Times.Once);
        }
    }

    [Fact]
    public async Task ScheduleSync_WhenDeleteFails_StillSetsNextSchedule()
    {
        var websiteCheckpoint = new WebsiteCheckpoint();

        var createScheduleResponse = Fixture.Build<CreateScheduleResponse>()
            .With(r => r.HttpStatusCode, HttpStatusCode.OK)
            .Create();
        AmazonScheduler.Setup(s => s.CreateScheduleAsync(It.IsAny<CreateScheduleRequest>(), default))
            .ReturnsAsync(createScheduleResponse)
            .Verifiable();

        var listSchedulesResponse = Fixture.Create<ListSchedulesResponse>();
        AmazonScheduler.Setup(s => s.ListSchedulesAsync(It.IsAny<ListSchedulesRequest>(), default))
            .ReturnsAsync(listSchedulesResponse)
            .Verifiable();

        AmazonScheduler.Setup(s => s.DeleteScheduleAsync(It.IsAny<DeleteScheduleRequest>(), default))
            .ThrowsAsync(new Exception())
            .Verifiable();

        await SyncScheduler.ScheduleSync(websiteCheckpoint);

        AmazonScheduler.Verify();
    }
}
