using FluentAssertions;
using HtmlAgilityPack;
using IndecScraper.Models;
using Xunit;

namespace IndecScraper.Tests.Models;

public class MonthCPITests
{
    public static readonly IEnumerable<object[]> MonthCPI_BuildsWithCorrectValues_TestData = new List<object[]>
    {
        new object[]
        {
            "<div class=\"col-lg-9\">" +
            "    <div style=\"font-size:14px;\">14/04/2023</div>" +
            "    <div class=\"mb-3\" style=\"font-size:16px;\"><span class=\"font-bold\">Índice de precios al consumidor</span></div>" +
            "    <div>" +
            "        <span class=\"copete\">El nivel general del Índice de Precios al Consumidor (IPC) representativo del total de" +
            "            hogares del" +
            "            país registró en marzo una variación de 7,7% con relación al mes anterior." +
            "" +
            "            <br><br>" +
            "            Escuchar Dato INDEC" +
            "            <br><br>" +
            "            <audio controls=\"controls\" preload=\"none\">" +
            "                <source src=\"full-result_files/ipc042023.mp3\" type=\"audio/mpeg\">" +
            "                Su navegador no soporta audio. Por favor actualice el mismo." +
            "            </audio></span>" +
            "    </div>" +
            "</div>",
            new DateTime(2023, 4, 14, 0, 0, 0, DateTimeKind.Utc),
            new DateTime(2023, 3, 1, 0, 0, 0, DateTimeKind.Utc),
            7.7m
        },
        new object[]
        {
            "<div class=\"col-lg-9\">" +
            "    <div style=\"font-size:14px;\">15/02/2018</div>" +
            "    <div class=\"mb-3\" style=\"font-size:16px;\"><span class=\"font-bold\">Índice de Precios al Consumidor</span></div>" +
            "    <div>" +
            "        <span class=\"copete\">El" +
            "            nivel general del Índice de Precios al Consumidor (IPC) representativo" +
            "            del total de hogares del país registró en el mes de enero una variación" +
            "            de 1,8% con relación al mes anterior.</span>" +
            "    </div>" +
            "</div>",
            new DateTime(2018, 2, 15, 0, 0, 0, DateTimeKind.Utc),
            new DateTime(2018, 1, 1, 0, 0, 0, DateTimeKind.Utc),
            1.8m
        }
    };
    [Theory]
    [MemberData(nameof(MonthCPI_BuildsWithCorrectValues_TestData))]
    public void MonthCPI_BuildsWithCorrectValues(string htmlNode, DateTime expectedDatePublished, DateTime expectedPeriod, decimal expectedIndex)
    {
        var doc = new HtmlDocument();
        doc.LoadHtml(htmlNode);
        var result = new MonthCPI(doc.DocumentNode.FirstChild);
        result.DatePublished.Should().Be(expectedDatePublished);
        result.Period.Should().Be(expectedPeriod);
        result.Index.Value.Should().Be(expectedIndex);
    }
}