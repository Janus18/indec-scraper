using FluentAssertions;
using HtmlAgilityPack;
using IndecScraper.Models;
using IndecScraper.Tests.TestUtils;
using Xunit;

namespace IndecScraper.Tests.Models;

public class WebsiteTests
{
    [Fact]
    public static void GetCPIEntries_ReturnsCorrectValues()
    {
        var htmlDoc = new HtmlDocument();
        htmlDoc.Load("Mock/full-result.html");
        var website = new Website(htmlDoc);
        var result = website.GetCPIEntries();
        result.Should().NotBeNullOrEmpty()
            .And.HaveCount(82)
            .And.Contain(x =>
                x.DatePublished == DateTimeUtils.FromYearMonthDay(2023, 4, 14) &&
                x.Period == DateTimeUtils.FromYearMonth(2023, 3) &&
                x.Index.Value == 7.7m
            )
            .And.Contain(x =>
                x.DatePublished == DateTimeUtils.FromYearMonthDay(2022, 12, 15) &&
                x.Period == DateTimeUtils.FromYearMonth(2022, 11) &&
                x.Index.Value == 4.9m
            )
            .And.Contain(x =>
                x.DatePublished == DateTimeUtils.FromYearMonthDay(2022, 5, 12) &&
                x.Period == DateTimeUtils.FromYearMonth(2022, 4) &&
                x.Index.Value == 6m
            )
            .And.Contain(x =>
                x.DatePublished == DateTimeUtils.FromYearMonthDay(2021, 9, 14) &&
                x.Period == DateTimeUtils.FromYearMonth(2021, 8) &&
                x.Index.Value == 2.5m
            )
            .And.Contain(x =>
                x.DatePublished == DateTimeUtils.FromYearMonthDay(2020, 7, 15) &&
                x.Period == DateTimeUtils.FromYearMonth(2020, 6) &&
                x.Index.Value == 2.2m
            )
            .And.Contain(x =>
                x.DatePublished == DateTimeUtils.FromYearMonthDay(2018, 8, 15) &&
                x.Period == DateTimeUtils.FromYearMonth(2018, 7) &&
                x.Index.Value == 3.1m
            );
        result.Select(x => x.Index.Value).Sum().Should().Be(281.7m);
    }

    [Fact]
    public static async Task AsString_ReturnsStringOfAsciiEncodedHtmlDocument()
    {
        var fileContent = await System.IO.File.ReadAllTextAsync("Mock/full-result.html", System.Text.Encoding.ASCII);

        var htmlDoc = new HtmlDocument();
        htmlDoc.Load("Mock/full-result.html");
        var website = new Website(htmlDoc);
        var encodedWebsite = website.AsString();

        encodedWebsite.Should().Be(fileContent);
    }
}