using AutoFixture;
using FluentAssertions;
using IndecScraper.Models;
using Xunit;

namespace IndecScraper.Tests.Models;

public static class WebsiteCheckpointTests
{
    private static readonly Fixture Fixture = new();

    [Fact]
    public static void Add_AddsCreatedAndUpdatedValues()
    {
        var samples = Enumerable.Range(0, 20).Select(_ => (Fixture.Create<int>() % 1000, Fixture.Create<int>() % 1000));
        var wc = new WebsiteCheckpoint();
        wc.Created.Should().Be(0);
        wc.Updated.Should().Be(0);
        foreach (var (created, updated) in samples)
        {
            var currentCreated = wc.Created;
            var currentUpdated = wc.Updated;
            wc.Add(created, updated);
            wc.Created.Should().Be(currentCreated + created);
            wc.Updated.Should().Be(currentUpdated + updated);
        }
    }

    [Fact]
    public static void NextExecution_IsOnTheTenthOfNextMonth_WhenWebsiteCheckpointIsNew()
    {
        var wc = new WebsiteCheckpoint(true);
        wc.Add(1, 0);
        wc.NextExecution.Should().Be(wc.Timestamp.AddMonths(1).AddDays(10 - wc.Timestamp.Day));
    }

    [Fact]
    public static void NextExecution_IsTomorrowWhenWebsiteCheckpointIsNotNew()
    {
        var wc = new WebsiteCheckpoint();
        wc.NextExecution.Should().BeCloseTo(DateTime.UtcNow.AddDays(1), new TimeSpan(0, 0, 5));
    }
}
