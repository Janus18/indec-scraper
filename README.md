![Pipeline](https://gitlab.com/Janus18/indec-scraper/badges/master/pipeline.svg?ignore_skipped=true) ![Release](https://gitlab.com/Janus18/indec-scraper/-/badges/release.svg)

# INDEC Scraper

Scrape INDEC reports to obtain and process data. This is a simple but
overengineered toy project.

Currently it only works with consumer price index (CPI) reports from INDEC.
These reports inform Argentina's month to month inflation.

This application stores every month's CPI value in a database and provides an API to
fetch them and calculate the overall inflation index in a given range, as well as
a website which consumes that API and offers a better visualization of data.

The synchronization between the application and the website happens automatically
each month, after being bootstrapped when the project is uploaded. You can read more
about that in the back-end's README file.

You can view the result here: https://indecscraper.aemona.top/

## High-level description

The application is made up of two projects, one for the back-end and other for
the front-end. You can find them in the `src` directory. Each of the projects contain
a README with more details on how to set up and run it.

Both projects are published and run with AWS services. The back-end runs in a Lambda
function, and the front is stored in a S3 bucket used as a website hosting.
To set up the infrastructure needed, you can use the Terraform's definitions in the
`terraform` directory. That directory also contains a README with more details of the
infrastructure.

## Navigate the project

[Back-end](https://gitlab.com/Janus18/indec-scraper/-/blob/master/src/IndecScraper)  
[Front-end](https://gitlab.com/Janus18/indec-scraper/-/blob/master/src/IndecScraper.UI)  
[Infrastructure](https://gitlab.com/Janus18/indec-scraper/-/blob/master/terraform)  
