using IndecScraper.Models;
using IndecScraper.Values;

namespace IndecScraper.Values;

public class AccumulatedMonthCPI
{
    public DateTime DatePublished { get; }

    public DateTime Period { get; }

    public Percentage Index { get; }

    public Percentage Accumulated { get; }

    public AccumulatedMonthCPI(MonthCPI monthCPI, Percentage accumulated)
    {
        DatePublished = monthCPI.DatePublished;
        Period = monthCPI.Period;
        Index = monthCPI.Index;
        Accumulated = monthCPI.Index.AddTo(accumulated);
    }
}
