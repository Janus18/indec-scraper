using System.Text.Json.Serialization;
using IndecScraper.Infrastructure.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;

namespace IndecScraper.Values;

[JsonConverter(typeof(PercentageJsonSerializer))]
public class Percentage
{
    public decimal Value { get; }

    private decimal Hundredth { get; }

    /// <summary>
    /// The value is intended to be the percentage value, e.g. 7.4 for 7.4%.
    /// </summary>
    /// <param name="value"></param>
    public Percentage(decimal value)
    {
        Value = value;
        Hundredth = value / 100;
    }

    public Percentage AddTo(Percentage x) => new Percentage(((x.Hundredth + 1) * (Hundredth + 1) - 1) * 100);

    public override string ToString() => $"{Value}%";

    public override bool Equals(object? obj) => obj is Percentage p && p.Value == Value;

    public override int GetHashCode() => Value.GetHashCode();

    public static bool operator ==(Percentage p1, Percentage p2) => p1.Equals(p2);

    public static bool operator !=(Percentage p1, Percentage p2) => !(p1 == p2);
}
