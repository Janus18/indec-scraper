
using System.Globalization;
using System.Text;

namespace IndecScraper.Values;

public record struct MonthsPeriod(DateTime from, DateTime to)
{
    private static Func<string, DateTime> ParseValue = (s) => DateTime.ParseExact(
        s, "yyyy-MM",
        CultureInfo.InvariantCulture,
        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal);

    public static MonthsPeriod Parse(string from, string to)
        => new MonthsPeriod(ParseValue(from), ParseValue(to));

    private bool PrintMembers(StringBuilder builder)
    {
        builder.Append($"{from.ToString("MM/yyyy")} - {to.ToString("MM/yyyy")}");
        return true;
    }
}
