using IndecScraper.Application.Configuration;

namespace IndecScraper.Values;

public record AwsSchedule(string DebugName, string GroupName, string Prefix, Func<ApplicationConfiguration, string> Body)
{
    public static AwsSchedule SyncSchedule = new(
        nameof(SyncSchedule),
        "default", 
        "indecscraper-schedule", 
        c => $"[\\\"Sync\\\", \\\"{c.Password}\\\"]");
    
    public static AwsSchedule PingSchedule = new(
        nameof(PingSchedule),
        "default", 
        "indecscraper-ping", 
        c => $"[\\\"Ping\\\", \\\"{c.Password}\\\"]");
}