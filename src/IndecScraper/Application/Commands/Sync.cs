using IndecScraper.Application.Commands.Contract;
using IndecScraper.Models;
using MediatR;

namespace IndecScraper.Application.Commands;

public class Sync : IRequest<WebsiteCheckpoint>, IProtectedCommand
{
    public string Password { get; }

    public Sync(string password)
    {
        Password = password;
    }
}
