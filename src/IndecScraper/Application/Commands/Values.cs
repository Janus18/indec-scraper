using IndecScraper.Values;
using MediatR;

namespace IndecScraper.Application.Commands;

public class Values : IRequest<IEnumerable<AccumulatedMonthCPI>>
{
    public MonthsPeriod Period { get; }
    
    public Values(string from, string to)  : this(MonthsPeriod.Parse(from, to))
    {
    }

    // We don't want this to be exposed to the Command.GetCommand method.
    private Values(MonthsPeriod period)
    {
        Period = period;
    }

    public static Values Make(MonthsPeriod monthsPeriod) => new Values(monthsPeriod);
}
