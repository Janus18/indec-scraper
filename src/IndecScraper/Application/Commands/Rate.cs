using IndecScraper.Values;
using MediatR;

namespace IndecScraper.Application.Commands;

public class Rate : IRequest<Percentage>
{
    public MonthsPeriod Period { get; }

    public Rate(string from, string to)
    {
        Period = MonthsPeriod.Parse(from, to);
    }
}
