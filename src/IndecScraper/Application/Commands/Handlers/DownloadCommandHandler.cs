using IndecScraper.Infrastructure.Interfaces;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class DownloadCommandHandler : IRequestHandler<Download, string>
{
    private readonly IWebReader webReader;
    
    public DownloadCommandHandler(
        IWebReader webReader
    )
    {
        this.webReader = webReader;
    }

    public async Task<string> Handle(Download request, CancellationToken cancellationToken)
    {
        var website = await webReader.ReadWebsite();
        return website.AsString();
    }
}
