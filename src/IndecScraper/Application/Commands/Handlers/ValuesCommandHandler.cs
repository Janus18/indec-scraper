using System.Collections.Immutable;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Values;
using MediatR;
using Serilog;

namespace IndecScraper.Application.Commands.Handlers;

public class ValuesCommandHandler : IRequestHandler<Values, IEnumerable<AccumulatedMonthCPI>>
{
    private readonly IDatabaseService databaseService;
    private readonly ILogger logger;

    public ValuesCommandHandler(
        IDatabaseService databaseService,
        ILogger logger
    )
    {
        this.databaseService = databaseService;
        this.logger = logger;
    }

    public async Task<IEnumerable<AccumulatedMonthCPI>> Handle(Values request, CancellationToken cancellationToken)
    {
        var months = await databaseService.GetMany<MonthCPI>(
            s => request.Period.from <= s.Period && s.Period <= request.Period.to);
        logger.Information("{count} values found in the period {period}", months.Count(), request.Period);
        return months
                .OrderBy(x => x.Period)
                .Aggregate(ImmutableList<AccumulatedMonthCPI>.Empty, (result, next) =>
                {
                    var acc = new AccumulatedMonthCPI(next, result.LastOrDefault()?.Accumulated ?? new Percentage(0));
                    return result.Add(acc);
                });
    }
}
