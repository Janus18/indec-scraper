using IndecScraper.Infrastructure.Implementation;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class CheckpointCommandHandler : IRequestHandler<Checkpoint, WebsiteCheckpoint?>
{
    private readonly IDatabaseService databaseService;

    public CheckpointCommandHandler(
        IDatabaseService databaseService
    )
    {
        this.databaseService = databaseService;
    }

    public async Task<WebsiteCheckpoint?> Handle(Checkpoint request, CancellationToken cancellationToken)
    {
        var orderBy = new DatabaseOrder<WebsiteCheckpoint>(c => c.Timestamp, false);
        return await databaseService.GetFirst<WebsiteCheckpoint>(orderBy: orderBy);
    }
}
