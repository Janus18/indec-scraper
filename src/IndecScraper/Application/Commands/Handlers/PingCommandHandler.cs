using IndecScraper.Application.Services;
using IndecScraper.Models;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class PingCommandHandler(
    IRequestHandler<Checkpoint, WebsiteCheckpoint?> checkpointHandler,
    ISyncScheduler syncScheduler
) : IRequestHandler<Ping, WebsiteCheckpoint?>
{
    public async Task<WebsiteCheckpoint?> Handle(Ping request, CancellationToken cancellationToken)
    {
        var checkpointTask = checkpointHandler.Handle(new Checkpoint(), cancellationToken);
        await syncScheduler.SchedulePing(DateTime.UtcNow.AddDays(1));
        return await checkpointTask;
    }
}