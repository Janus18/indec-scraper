using IndecScraper.Application.Services;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class SyncCommandHandler : IRequestHandler<Sync, WebsiteCheckpoint>
{
    private readonly IWebReader webReader;
    private readonly ICPISynchronization cpiSynchronization;
    private readonly IDatabaseService databaseService;
    private readonly ISyncScheduler syncScheduler;

    public SyncCommandHandler(
        IWebReader webReader,
        ICPISynchronization cpiSynchronization,
        IDatabaseService databaseService,
        ISyncScheduler syncScheduler
    )
    {
        this.webReader = webReader;
        this.cpiSynchronization = cpiSynchronization;
        this.databaseService = databaseService;
        this.syncScheduler = syncScheduler;
    }

    public async Task<WebsiteCheckpoint> Handle(Sync request, CancellationToken cancellationToken)
    {
        var result = await SyncAndGetCheckpoint();
        await syncScheduler.ScheduleSync(result);
        return result;
    }

    public async Task<WebsiteCheckpoint> SyncAndGetCheckpoint()
    {
        var website = await webReader.ReadWebsite();
        var transaction = await databaseService.CreateTransaction();
        var result = await cpiSynchronization.SyncWebsite(website, transaction);
        await databaseService.Save(result, transaction);
        await transaction.Commit();
        return result;
    }
}
