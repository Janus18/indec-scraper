using IndecScraper.Application.Services;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class ConfigurationCommandHandler : IRequestHandler<Configuration, string>
{
    private readonly IBuildConfiguration buildConfiguration;

    public ConfigurationCommandHandler(IBuildConfiguration buildConfiguration)
    {
        this.buildConfiguration = buildConfiguration;
    }

    public Task<string> Handle(Configuration request, CancellationToken cancellationToken)
        => Task.FromResult(buildConfiguration.Name()!);
}
