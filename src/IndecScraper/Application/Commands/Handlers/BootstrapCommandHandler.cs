using IndecScraper.Application.Services;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class BootstrapCommandHandler(ISyncScheduler syncScheduler) : IRequestHandler<Bootstrap, bool>
{
    public async Task<bool> Handle(Bootstrap request, CancellationToken cancellationToken)
    {
        var results = await Task.WhenAll(
            syncScheduler.ScheduleSync(null),
            syncScheduler.SchedulePing(DateTime.UtcNow.AddMinutes(1))
        );
        return results.All(x => x);
    }
}
