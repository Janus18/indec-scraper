using IndecScraper.Values;
using MediatR;

namespace IndecScraper.Application.Commands.Handlers;

public class RateCommandHandler : IRequestHandler<Rate, Percentage>
{
    private readonly IRequestHandler<Values, IEnumerable<AccumulatedMonthCPI>> valuesHandler;

    public RateCommandHandler(
        IRequestHandler<Values, IEnumerable<AccumulatedMonthCPI>> valuesHandler
    )
    {
        this.valuesHandler = valuesHandler;
    }

    public async Task<Percentage> Handle(Rate request, CancellationToken cancellationToken)
    {
        var monthsInflation = await valuesHandler.Handle(Values.Make(request.Period), cancellationToken);
        return monthsInflation.LastOrDefault()?.Accumulated ?? new Percentage(0);
    }
}
