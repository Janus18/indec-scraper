using IndecScraper.Application.Commands.Contract;
using IndecScraper.Application.Services;
using MediatR;

namespace IndecScraper.Application.Commands.Behaviors;

public class AuthorizationBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : notnull, IRequest<TResponse>
{
    private readonly IAuthorizer _authorizer;

    public AuthorizationBehavior(IAuthorizer authorizer)
    {
        _authorizer = authorizer;
    }
    
    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        if (request is IProtectedCommand protectedCommand && !_authorizer.IsAuthorized(protectedCommand.Password))
        {
            throw new UnauthorizedAccessException();
        }

        return await next();
    }
}