using IndecScraper.Application.Commands.Contract;
using IndecScraper.Models;
using MediatR;

namespace IndecScraper.Application.Commands;

public record Ping(string Password) : IRequest<WebsiteCheckpoint?>, IProtectedCommand;
