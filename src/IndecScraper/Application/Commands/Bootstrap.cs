using IndecScraper.Application.Commands.Contract;
using MediatR;

namespace IndecScraper.Application.Commands;

public class Bootstrap : IRequest<bool>, IProtectedCommand
{
    public string Password { get; }

    public Bootstrap(string password)
    {
        Password = password;
    }
}
