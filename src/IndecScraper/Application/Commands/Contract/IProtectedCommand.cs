namespace IndecScraper.Application.Commands.Contract;

public interface IProtectedCommand
{
    string Password { get; }
}