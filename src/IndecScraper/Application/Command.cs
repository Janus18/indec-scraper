using MediatR;

namespace IndecScraper.Application;

public class Command
{
    private static readonly IEnumerable<Type> AppCommands = typeof(Command).Assembly
        .GetTypes()
        .Where(t => t.FindInterfaces((ty, x) => ty == typeof(IBaseRequest), null).Any())
        .Where(t => t is { IsInterface: false, IsAbstract: false })
        .ToList();

    public static IBaseRequest GetCommand(string[] args)
    {
        if (args == null || !args.Any())
        {
            throw new ArgumentException("Wrong number of arguments (at least one expected).");
        }
        var commandType = AppCommands.First(t => t.Name == args[0]);
        var command = Activator.CreateInstance(commandType, args.Skip(1).Select(x => (object)x).ToArray());
        return (IBaseRequest)command!;
    }
}
