using IndecScraper.Application.Commands.Behaviors;
using IndecScraper.Application.Services;
using IndecScraper.Application.Services.Implementation;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace IndecScraper.Application;

public static class DependencyInjection
{
    public static IServiceCollection Inject(IServiceCollection services)
    {
        services.AddScoped<ICPISynchronization, CPISynchronization>();
        services.AddScoped<ISyncScheduler, SyncScheduler>();
        services.AddScoped<IAuthorizer, Authorizer>();
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(AuthorizationBehavior<,>));
        services.AddMediatR(typeof(DependencyInjection).Assembly);
        return services;
    }
}
