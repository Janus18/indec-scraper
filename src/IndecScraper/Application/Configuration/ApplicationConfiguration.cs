
namespace IndecScraper.Application.Configuration;

public class ApplicationConfiguration
{
    public string WebUrl { get; set; } = "";
    public string DatabaseConnectionString { get; set; } = "";
    public string LambdaArn { get; set; } = "";
    public string EventBridgeRoleArn { get; set; } = "";
    public string Password { get; set; } = "";
}
