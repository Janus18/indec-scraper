namespace IndecScraper.Application.Services;

public interface IAuthorizer
{
    bool IsAuthorized(string password);
}