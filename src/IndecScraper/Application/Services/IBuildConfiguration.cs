namespace IndecScraper.Application.Services;

public interface IBuildConfiguration
{
    string? Name();
}
