using IndecScraper.Models;

namespace IndecScraper.Application.Services;

public interface ISyncScheduler
{
    Task<bool> ScheduleSync(WebsiteCheckpoint? lastWebsiteCheckpoint);

    Task<bool> SchedulePing(DateTime next);
}
