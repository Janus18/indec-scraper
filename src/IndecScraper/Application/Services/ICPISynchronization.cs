using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Models.Interfaces;

namespace IndecScraper.Application.Services;

public interface ICPISynchronization
{
    Task<WebsiteCheckpoint> SyncWebsite(IWebsite website, ITransaction? transaction);
}
