using IndecScraper.Common;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Models.Interfaces;
using Serilog;

namespace IndecScraper.Application.Services.Implementation;

public class CPISynchronization : ICPISynchronization
{
    private readonly IDatabaseService databaseService;
    private readonly ILogger logger;

    public CPISynchronization(
        IDatabaseService databaseService,
        ILogger logger
    )
    {
        this.databaseService = databaseService;
        this.logger = logger;
    }

    public async Task<WebsiteCheckpoint> SyncWebsite(IWebsite website, ITransaction? transaction)
    {
        var monthCPIs = website.GetCPIEntries();
        logger.Information("{count} CPI entries were read", monthCPIs.Count);
        var batches = monthCPIs.Partition(50);
        var result = new WebsiteCheckpoint(true);
        foreach (var batch in batches)
        {
            var batchResult = await SyncIndexes(batch, transaction);
            result.Add(batchResult.Added, batchResult.Updated);
        }
        return result;
    }

    private record struct SyncResult(int Added, int Updated);

    private async Task<SyncResult> SyncIndexes(
        IEnumerable<MonthCPI> cpis,
        ITransaction? transaction = null
    )
    {
        var result = new SyncResult(0, 0);
        var periods = cpis.Select(x => x.Period).ToList();
        var dbCpis = await databaseService.GetMany<MonthCPI>(x => periods.Contains(x.Period), transaction);
        foreach (var cpi in cpis)
        {
            var dbCpi = dbCpis.FirstOrDefault(x => x.Period == cpi.Period);
            result = dbCpi is null
                ? await AddMonthCPI(transaction, result, cpi)
                : await UpdateMonthCPI(transaction, result, cpi, dbCpi);
        }
        return result;
    }

    private async Task<SyncResult> UpdateMonthCPI(ITransaction? transaction, SyncResult result, MonthCPI cpi, MonthCPI dbCpi)
    {
        if (dbCpi.Index == cpi.Index) {
            return result;
        }
        result = new SyncResult(result.Added, result.Updated + 1);
        dbCpi.Index = cpi.Index;
        await databaseService.Update(dbCpi, transaction);
        return result;
    }

    private async Task<SyncResult> AddMonthCPI(ITransaction? transaction, SyncResult result, MonthCPI cpi)
    {
        result = new SyncResult(result.Added + 1, result.Updated);
        await databaseService.Save(cpi, transaction);
        return result;
    }
}
