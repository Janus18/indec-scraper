using IndecScraper.Application.Configuration;
using Microsoft.Extensions.Options;

namespace IndecScraper.Application.Services.Implementation;

public class Authorizer : IAuthorizer
{
    private readonly ApplicationConfiguration _configuration;
    
    public Authorizer(IOptions<ApplicationConfiguration> options)
    {
        _configuration = options.Value;
    }
    
    public bool IsAuthorized(string password)
    {
        return _configuration.Password == password;
    }
}