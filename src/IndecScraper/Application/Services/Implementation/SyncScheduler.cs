using System.Net;
using System.Text.Json;
using Amazon.Scheduler;
using Amazon.Scheduler.Model;
using IndecScraper.Application.Configuration;
using IndecScraper.Common;
using IndecScraper.Models;
using IndecScraper.Values;
using Microsoft.Extensions.Options;
using Serilog;

namespace IndecScraper.Application.Services.Implementation;

public class SyncScheduler : ISyncScheduler
{
    private readonly IAmazonScheduler _schedulerClient;
    private readonly ApplicationConfiguration _configuration;
    private readonly ILogger _logger;

    private const string InvocationInput = "{ \"resource\": \"/\", \"path\": \"/\", \"httpMethod\": \"POST\", \"requestContext\": { \"resourcePath\": \"/\", \"httpMethod\": \"POST\", \"path\": \"/Prod/\" }, \"headers\": { \"accept\": \"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\", \"accept-encoding\": \"gzip, deflate, br\", \"Host\": \"70ixmpl4fl.execute-api.us-east-2.amazonaws.com\", \"User-Agent\": \"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36\", \"X-Amzn-Trace-Id\": \"Root=1-5e66d96f-7491f09xmpl79d18acf3d050\" }, \"multiValueHeaders\": { \"accept\": [ \"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9\" ], \"accept-encoding\": [ \"gzip, deflate, br\" ] }, \"queryStringParameters\": null, \"multiValueQueryStringParameters\": null, \"pathParameters\": null, \"stageVariables\": null, \"body\": \"{BODY}\", \"isBase64Encoded\": false }";

    public SyncScheduler(
        IAmazonScheduler schedulerClient,
        IOptions<ApplicationConfiguration> options,
        ILogger logger
    )
    {
        _schedulerClient = schedulerClient;
        _configuration = options.Value;
        _logger = logger;
    }

    public Task<bool> ScheduleSync(WebsiteCheckpoint? websiteCheckpoint)
        => Schedule(AwsSchedule.SyncSchedule, websiteCheckpoint?.NextExecution ?? DateTime.UtcNow.AddSeconds(30));

    public Task<bool> SchedulePing(DateTime next)
        => Schedule(AwsSchedule.PingSchedule, next);

    private async Task<bool> Schedule(AwsSchedule schedule, DateTime time)
    {
        await RemovePastSchedules(schedule);
        var scheduleRequest = ScheduleRequest(schedule, time);
        var response = await _schedulerClient.CreateScheduleAsync(scheduleRequest);
        var serializedResponse = JsonSerializer.Serialize(response, JsonOptions.Indented);
        if (response.HttpStatusCode is HttpStatusCode.OK or HttpStatusCode.NoContent)
        {
            _logger.Information("Schedule {name} created successfully. Response: {response}", 
                schedule.DebugName, 
                serializedResponse);
            return true;
        }

        _logger.Error("Error creating schedule {name}. Response {response}", 
            schedule.DebugName, 
            serializedResponse);
        return false;
    }

    private CreateScheduleRequest ScheduleRequest(AwsSchedule schedule, DateTime time) => new()
    {
        Name = $"{schedule.Prefix}-{Guid.NewGuid()}",
        GroupName = schedule.GroupName,
        ScheduleExpression = $"at({time:yyyy-MM-ddTHH:mm:ss})",
        ScheduleExpressionTimezone = "Etc/UTC",
        State = ScheduleState.ENABLED,
        FlexibleTimeWindow = new FlexibleTimeWindow { Mode = FlexibleTimeWindowMode.FLEXIBLE, MaximumWindowInMinutes = 2 },
        Target = new Target
        {
            Arn = _configuration.LambdaArn,
            RetryPolicy = new RetryPolicy
            {
                MaximumRetryAttempts = 3,
                MaximumEventAgeInSeconds = 7200
            },
            RoleArn = _configuration.EventBridgeRoleArn,
            Input = InvocationInput.Replace("{BODY}", schedule.Body(_configuration))
        }
    };

    private async Task RemovePastSchedules(AwsSchedule schedule)
    {
        try
        {
            var schedulesResponse = await _schedulerClient.ListSchedulesAsync(new ListSchedulesRequest
            {
                GroupName = schedule.GroupName,
                NamePrefix = schedule.Prefix
            });
            foreach (var sched in schedulesResponse.Schedules)
            {
                await _schedulerClient.DeleteScheduleAsync(new DeleteScheduleRequest
                {
                    GroupName = schedule.GroupName,
                    Name = sched.Name
                });
            }
            _logger.Information("Past schedules were deleted. Group: {group}, prefix: {prefix}.", 
                schedule.GroupName,
                schedule.Prefix);
        }
        catch (Exception ex)
        {
            _logger.Error(ex, "Error deleting past schedules. Group: {group}, prefix: {prefix}.", 
                schedule.GroupName, 
                schedule.Prefix);
        }
    }
}
