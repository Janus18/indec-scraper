using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace IndecScraper;

public static class EntryPoint
{
    public static IMediator GetMediator()
    {
        var serviceProvider = CreateServices();
        var scope = serviceProvider.CreateScope();
        return scope.ServiceProvider.GetService<IMediator>()!;
    }

    private static IServiceProvider CreateServices()
    {
        var services = new ServiceCollection();
        IndecScraper.Application.DependencyInjection.Inject(services);
        IndecScraper.Infrastructure.DependencyInjection.Inject(services);
        return services.BuildServiceProvider();
    }
}
