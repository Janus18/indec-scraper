using HtmlAgilityPack;
using IndecScraper.Models.Interfaces;

namespace IndecScraper.Models;

public class Website : IWebsite
{
    private const string CPIEntryTitle = "&#205;ndice de precios al consumidor";

    private readonly HtmlDocument document;

    public Website(HtmlDocument document)
    {
        this.document = document;
    }

    public IList<MonthCPI> GetCPIEntries() => document
        .DocumentNode
        .Descendants("span")
        .Where(n => n.InnerText.Equals(Website.CPIEntryTitle, StringComparison.InvariantCultureIgnoreCase))
        .Select(n => n.ParentNode.ParentNode)
        .Select(n => new MonthCPI(n)).ToList();

    public string AsString()
    {
        var stream = new MemoryStream();
        document.Save(stream, System.Text.Encoding.ASCII);
        return System.Text.Encoding.ASCII.GetString(stream.ToArray());
    }
}
