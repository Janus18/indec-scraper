using System.Globalization;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using IndecScraper.Models.Interfaces;
using IndecScraper.Values;

namespace IndecScraper.Models;

public class MonthCPI : IEntity
{
    public string Id { get; private set; } = "";

    private static readonly Regex _indexRegex = new Regex(@"(\d*,\d*%)");

    public DateTime DatePublished { get; set; }

    public DateTime Period { get; set; }

    public Percentage Index { get; set; }

    public MonthCPI(DateTime datePublished, Percentage index, DateTime period)
    {
        DatePublished = datePublished;
        Index = index;
        Period = period;
    }

    public MonthCPI(HtmlNode htmlNode)
    {
        DatePublished = ParseHtmlDate(GetDate(htmlNode));
        Index = new Percentage(ParseIndex(FindIndex(GetBody(htmlNode))));
        Period = GetPeriod(DatePublished);
    }

    private static string GetDate(HtmlNode htmlNode)
        => htmlNode.ChildNodes.First(c => c.Name == "div").InnerText;

    private static string GetBody(HtmlNode htmlNode)
        => htmlNode.ChildNodes.Last(c => c.Name == "div").ChildNodes.First(c => c.Name == "span").InnerText;

    private static DateTime ParseHtmlDate(string htmlDate)
        => DateTime.ParseExact(
            htmlDate,
            "dd/MM/yyyy",
            CultureInfo.InvariantCulture,
            DateTimeStyles.AdjustToUniversal | DateTimeStyles.AssumeUniversal
        );

    private static string FindIndex(string htmlBody)
        =>  _indexRegex.Matches(htmlBody).SelectMany(m => m.Groups.Values.Select(g => g.Value)).First();

    /// <summary>
    /// Parses string which can be matched by the _indexRegex.
    /// </summary>
    /// <param name="index"></param>
    private static decimal ParseIndex(string index) 
        => decimal.Parse(
            index.TrimEnd('%'),
            NumberStyles.AllowDecimalPoint,
            Common.CultureInfo.EsCulture
        );

    private static DateTime GetPeriod(DateTime datePublished)
        => new DateTime(
            datePublished.Year,
            datePublished.Month,
            1, 0, 0, 0,
            DateTimeKind.Utc
        ).AddMonths(-1);

    public override string ToString() => $"{Period:yyyy-MM}: {Index:0.00}";
}