namespace IndecScraper.Models.Interfaces;

public interface IEntity
{
    string Id { get; }
}
