namespace IndecScraper.Models.Interfaces;

public interface IWebsite
{
    IList<MonthCPI> GetCPIEntries();
    
    string AsString();
}
