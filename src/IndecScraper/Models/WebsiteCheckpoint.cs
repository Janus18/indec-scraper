using IndecScraper.Models.Interfaces;

namespace IndecScraper.Models;

public class WebsiteCheckpoint : IEntity
{
    public string Id { get; private set; } = "";

    public DateTime Timestamp { get; private set; }

    public int Created { get; private set; }

    public int Updated { get; private set; }

    public bool IsNew { get; private set; }

    public WebsiteCheckpoint() : this(false)
    {
    }

    public WebsiteCheckpoint(bool isNew = false)
    {
        Created = 0;
        Timestamp = DateTime.UtcNow;
        Updated = 0;
        IsNew = isNew;
    }

    public void Add(int created, int updated)
    {
        Created += created;
        Updated += updated;
    }

    /// <summary>
    /// Next sync will be on the 10th of the next month if the current sync went well.
    /// Otherwise we'll try again tomorrow.
    /// </summary>
    public DateTime NextExecution => IsNew && Created >= 1
        ? Timestamp.AddMonths(1).AddDays(10 - Timestamp.Day)
        : DateTime.UtcNow.AddDays(1);
}
