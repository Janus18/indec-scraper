# INDEC Scraper Back-End

This is the back-end project for the app. It currently uses dotnet v7,
a Mongo DB as storage, AWS services like Lambda and Secrets Manager when deployed,
and in it's core it uses [HtmlAgilityPack](https://html-agility-pack.net/) for the
scraping. For production, the project is built into a Docker container.

The project is based on these [AWS Lambda blueprints](https://github.com/aws/aws-lambda-dotnet/tree/master/Blueprints/BlueprintDefinitions/vs2022/EmptyFunction-Image/template/src/BlueprintBaseName.1).

## How it works

The steps the application follows in it's cycle are:
* Scrape a website with PCI information. The shape of data is hardcoded deep in the models, so any change in it will
break the parsing, but well, the data is intended to be read by humans.
* Store the data in a database, month by month.
* Also store a record of the synchronization in the database. The WebsiteCheckpoint knows how many PCI records were
created and updated in a run.
* After everything is saved in the DB, a scheduled event is registered in AWS to execute the synchronization again,
so the application can keep itself updated. Because the PCI is informed once a month, once we
obtain the current month's value, there's no need to sync again until the next month. That's why the event will fire
the tenth of the next month if the synchronization was successful, but it'll fire in a day if it wasn't, because we may have been too
early to fetch the new value.
* With the event registered, there's nothing more to do until the next cycle but use the fetching commands to
view the stored data.

Note the synchronization related commands (e.g., Bootstrap and Sync) are password protected, avoiding possible
misuse, which could cause too much load on the Lambda and too many AWS schedules created.

## Set up

The project can be used from the terminal or as an AWS Lambda function, the
exposed commands are identical for both cases.

Firstly, you'll need a Mongo DB database set up, and it's connection string;
the operations on the database implemented make use of transactions, so it cannot be a standalone deployment.
You'll either need to use Atlas or configure a local replica set. Add it's connection
string to the app's configuration.

### Configuration
The app obtains the configuration values from three possible sources, but it
also depends on the project's build configuration, which can be set with
`dotnet build --configuration` and `dotnet run --configuration`. I usually use
`user-secrets` or `appsettings.json` for `Debug`, and AWS Secrets Manager
in `Release` mode, so these are the supported configuration providers for each
config.

The Dockerfile builds the project with Release configuration, so it's only
intended to be used for the AWS Lambda.

Aside from the database connection string, you'll need:

* The web URL to scrape the data from. I'm currently using
[this link](https://www.indec.gob.ar/Institucional/Indec/InformesTecnicos/31 "INDEC reports").
* The Lambda function ARN.
* The ARN of an IAM role which will be used by EventBridge, and should be able to invoke the Lambda.
* A personal password to restrict access to some commands.

In general, the application configuration values are those declared in the
class `IndecScraper.Application.Configuration.ApplicationConfiguration`, you can refer
to it in case I miss to update this document.

### Deploy
Once the configuration is taken care of, and if you intend to test the project
as a Lambda function, you'll need to deploy it. Just run:
```dotnet lambda deploy-function -fn indecscraper```
You might need to provide credentials in case you haven't yet configured the AWS CLI,
in any case refer to the
[.NET Core global tools for AWS](https://aws.amazon.com/blogs/developer/net-core-global-tools-for-aws/).

## Execute

Whether you use the app from the terminal or as an Lambda function, there is a set of
commands you can try out. The commands are given as arguments to the program. The first
argument is the name of the command, the following args are passed down to that command,
if it takes any.

### Terminal

So, for example, to issue the Rate command, which takes two arguments, in the terminal:
```dotnet run Rate 2020-01 2020-12```
And you'll see the corresponding rate printed there.

### Lambda

The Lambda function entry point is located in the file `Function.cs`.
It's intended to be used from an API Gateway, so it accepts an HTTP request as an argument, and returns an HTTP response.
To invoke it, you can either wire an API Gateway endpoint as intended or simulate the gateway's request in the *Test* tab of the Lambda function in the AWS console. The payload of the request must be an array of strings, these strings are the arguments to the function as explained before. A valid payload could be
```[ "Rate", "2020-01", "2020-12" ]```

### The commands

There are currently seven commands:

* **Bootstrap**: As it name indicates, the purpose is to bootstrap the application, more precisely the
synchronization flow. The command will create an AWS schedule set to execute the sync command in 30 seconds.
* **Checkpoint**: Retrieves the lates WebsiteCheckpoint from the DB, which informs us the last time the
sync took place.
* **Configuration**: Just prints the configuration used to build the project, which will have an
effect on the config providers used, as explained before. This is just for debugging purposes.
* **Download**: Returns the contents of the web indicated in the configuration's WebUrl value. This
is also intended for debugging.
* **Rate**: Returns the inflation's index in a monthly range. You need to provide it two month values
with the format `yyyy-MM` (dotnet's DateTime format), e.g., `Rate 2023-01 2023-04`. If you omit or
pass an invalid month, an exception will be thrown. The values will be read from the database, so make
sure to Sync before using this command.
* **Sync**: Reads the web indicated in the configuration, and synchronizes the values stored in the
database with those parsed from the web. The sync implies updating values if changed, and adding those
which are not saved yet. The result indicates how many values were added and updated.
* **Values**: Returns an array of inflation indexes in the range indicated by the arguments. Just like
with **Rate** before, two months are parsed with the format `yyyy-MM`.
