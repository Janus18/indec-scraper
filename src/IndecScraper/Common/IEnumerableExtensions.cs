
namespace IndecScraper.Common;

public static class IEnumerableExtensions
{
    public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> xs, int itemsByPart)
    {
        if (itemsByPart == 0)
            return new List<IEnumerable<T>>();
        int parts = (int)double.Ceiling(((double)xs.Count()) / itemsByPart);
        return Enumerable.Range(0, parts)
            .Select(i => xs.Skip(i * itemsByPart).Take(itemsByPart))
            .ToList();
    }
}
