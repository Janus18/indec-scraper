using System.Globalization;

namespace IndecScraper.Common;

public static class CultureInfo
{
    private static readonly System.Globalization.CultureInfo EsArCulture =
        System.Globalization.CultureInfo.GetCultureInfo("es-AR");
    
    private static readonly System.Globalization.CultureInfo EsUsCulture =
        System.Globalization.CultureInfo.GetCultureInfo("es-US");

    public static System.Globalization.CultureInfo EsCulture = EsArCulture.IsValidCulture() ? EsArCulture : EsUsCulture;

    public static bool IsValidCulture(this System.Globalization.CultureInfo culture)
    {
        var isCustom = (culture.CultureTypes & CultureTypes.UserCustomCulture ) == CultureTypes.UserCustomCulture;
        return !isCustom;
    }
}