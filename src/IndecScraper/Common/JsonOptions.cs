using System.Text.Json;

namespace IndecScraper.Common;

public class JsonOptions
{
    public static JsonSerializerOptions Indented = new() { WriteIndented = true };
}