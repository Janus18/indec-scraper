using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Amazon.Lambda.Serialization.SystemTextJson;
using IndecScraper.Infrastructure.Serializers;
using System.Text.Json.Serialization;
using IndecScraper.Infrastructure;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(SourceGeneratorLambdaJsonSerializer<IndecScraper.HttpApiJsonSerializerContext>))]

namespace IndecScraper;

[JsonSerializable(typeof(APIGatewayHttpApiV2ProxyRequest))]
[JsonSerializable(typeof(APIGatewayHttpApiV2ProxyResponse))]
public partial class HttpApiJsonSerializerContext : JsonSerializerContext
{
}

public class Function
{
    /// <summary>
    /// </summary>
    /// <param name="request"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task<APIGatewayHttpApiV2ProxyResponse> FunctionHandler(APIGatewayHttpApiV2ProxyRequest request, ILambdaContext context)
    {
        try
        {
            var result = await Process(request);
            return new APIGatewayHttpApiV2ProxyResponse
            {
                StatusCode = 200,
                Body = JsonSerializer.Serialize(result),
                Headers = new Dictionary<string, string>
                {
                    { "content-type", "application/json" }
                }
            };
        }
        catch (UnauthorizedAccessException)
        {
            Logger.Instance.Error("Unauthorized");
            return new APIGatewayHttpApiV2ProxyResponse { StatusCode = 401 };
        }
        catch (Exception ex)
        {
            Logger.Instance.Error(ex, "Unexpected exception");
            return new APIGatewayHttpApiV2ProxyResponse { StatusCode = 500 };
        }
    }

    private static async Task<object?> Process(APIGatewayHttpApiV2ProxyRequest request)
    {
        Logger.Instance.Information("Processing request with body {body}", request.Body);
        var deserializedBody = System.Text.Json.JsonSerializer.Deserialize<string[]>(request.Body)
            ?? throw new ArgumentException($"Body has an invalid format: {request.Body}");
        var command = Application.Command.GetCommand(deserializedBody);
        var mediator = EntryPoint.GetMediator();
        return await mediator.Send(command);
    }
}
