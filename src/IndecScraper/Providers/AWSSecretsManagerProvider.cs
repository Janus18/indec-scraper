using Amazon;
using Amazon.SecretsManager;
using Amazon.SecretsManager.Model;
using Microsoft.Extensions.Configuration;

namespace IndecScraper.Providers;

public class AWSSecretsManagerProvider : ConfigurationProvider
{
    private readonly string secretName;
    private readonly string region;

    public AWSSecretsManagerProvider(string secretName, string region)
    {
        this.secretName = secretName;
        this.region = region;
    }

    public override void Load()
    {
        string secret = GetSecret().Result;
        var data =  System.Text.Json.JsonSerializer.Deserialize<Dictionary<string, string?>>(secret)
            ?? new Dictionary<string, string?>();
        Serilog.Log.Logger.Information("Configuration values found in AWS Secrets Manager ({keys})", string.Join(", ", data.Keys));
        Data = data;
    }

    private async Task<string> GetSecret()
    {
        IAmazonSecretsManager client = new AmazonSecretsManagerClient(RegionEndpoint.GetBySystemName(region));
        GetSecretValueRequest request = new GetSecretValueRequest { SecretId = secretName };
        GetSecretValueResponse response = await client.GetSecretValueAsync(request);
        return response.SecretString;
    }
}