
using Microsoft.Extensions.Configuration;

namespace IndecScraper.Providers;

public static class ConfigurationBuilderExtensions
{
    public static IConfigurationBuilder AddAWSSecretsManager(this IConfigurationBuilder builder, string secretName, string region)
        => builder.Add(new AWSSecretsManagerConfigurationSource(secretName, region));
}
