
using Microsoft.Extensions.Configuration;

namespace IndecScraper.Providers;

public class AWSSecretsManagerConfigurationSource : IConfigurationSource
{
    private readonly string secretName;
    private readonly string region;

    public AWSSecretsManagerConfigurationSource(string secretName, string region)
    {
        this.secretName = secretName;
        this.region = region;
    }

    public IConfigurationProvider Build(IConfigurationBuilder builder)
        => new AWSSecretsManagerProvider(secretName, region);
}
