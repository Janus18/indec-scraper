using IndecScraper.Infrastructure.Interfaces;
using MongoDB.Driver;

namespace IndecScraper.Infrastructure.Implementation;

public class Transaction : ITransaction
{
    public IClientSessionHandle Handle { get; }

    public Transaction(IClientSessionHandle sessionHandle)
    {
        Handle = sessionHandle;
        sessionHandle.StartTransaction();
    }

    public async Task Commit()
    {
        await Handle.CommitTransactionAsync();
        Handle.Dispose();
    }

    public async Task Rollback()
    {
        await Handle.AbortTransactionAsync();
        Handle.Dispose();
    }
}