using HtmlAgilityPack;
using IndecScraper.Application.Configuration;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Models;
using IndecScraper.Models.Interfaces;
using Microsoft.Extensions.Options;
using Serilog;

namespace IndecScraper.Infrastructure.Implementation;

public class WebReader : IWebReader
{
    private readonly ApplicationConfiguration configuration;
    private readonly ILogger logger;

    public WebReader(
        IOptions<ApplicationConfiguration> options,
        ILogger logger
    )
    {
        configuration = options.Value;
        this.logger = logger;
    }

    public Task<HtmlDocument> Read()
    {
        logger.Information("Loading web {url}", configuration.WebUrl);
        var web = new HtmlWeb();
        return web.LoadFromWebAsync(configuration.WebUrl);
    }

    public async Task<IWebsite> ReadWebsite()
    {
        logger.Information("Loading web {url}", configuration.WebUrl);
        var web = new HtmlWeb();
        var document = await web.LoadFromWebAsync(configuration.WebUrl);
        return new Website(document);
    }
}