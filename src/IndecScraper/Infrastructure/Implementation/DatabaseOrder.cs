using System.Linq.Expressions;

namespace IndecScraper.Infrastructure.Implementation;

public class DatabaseOrder<T>
{
    public Expression<Func<T, object>> Field { get; }
    public bool Ascending { get; }

    public DatabaseOrder(Expression<Func<T, object>> field, bool ascending)
    {
        Field = field;
        Ascending = ascending;
    }
}
