using System.Linq.Expressions;
using IndecScraper.Application.Configuration;
using IndecScraper.Models.Interfaces;
using IndecScraper.Infrastructure.Interfaces;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace IndecScraper.Infrastructure.Implementation;

public class DatabaseService : IDatabaseService
{
    private readonly IMongoDatabase db;

    public DatabaseService(IOptions<ApplicationConfiguration> options)
    {
        var dbClient = new MongoClient(options.Value.DatabaseConnectionString);
        db = dbClient.GetDatabase(nameof(IndecScraper));
    }

    public async Task<ITransaction> CreateTransaction()
    {
        var sessionHandle = await db.Client.StartSessionAsync();
        return new Transaction(sessionHandle);
    }

    public async Task<T?> GetFirst<T>(
        Expression<Func<T, bool>>? filter = null,
        DatabaseOrder<T>? orderBy = null,
        ITransaction? transaction = null
    )  where T : IEntity
    {
        PipelineDefinition<T, T> pipeline = new EmptyPipelineDefinition<T>();
        if (filter != null)
        {
            pipeline = pipeline.Match(new FilterDefinitionBuilder<T>().Where(filter));
        }
        if (orderBy != null)
        {
            var builder = new SortDefinitionBuilder<T>();
            pipeline = pipeline.Sort(orderBy.Ascending ? builder.Ascending(orderBy.Field) : builder.Descending(orderBy.Field));
        }
        var cursor = transaction is null
            ? await GetCollection<T>().AggregateAsync(pipeline)
            : await GetCollection<T>().AggregateAsync(transaction.Handle, pipeline);
        return await cursor.FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<T>> GetMany<T>(
        Expression<Func<T, bool>> filter,
        ITransaction? transaction = null
    ) where T : IEntity
    {
        var filterBuilder = new FilterDefinitionBuilder<T>();
        var builtFilter = filterBuilder.Where(filter);
        var cursor = transaction is null
            ? await GetCollection<T>().FindAsync(builtFilter)
            : await GetCollection<T>().FindAsync(transaction.Handle, builtFilter);
        return await cursor.ToListAsync();
    }

    public Task Save<T>(T value, ITransaction? transaction = null) where T : IEntity
    {
        return transaction is null
            ? GetCollection<T>().InsertOneAsync(value)
            : GetCollection<T>().InsertOneAsync(transaction.Handle, value);
    }

    public Task SaveMany<T>(IEnumerable<T> values, ITransaction? transaction = null) where T : IEntity
    {
        return transaction is null
            ? GetCollection<T>().InsertManyAsync(values)
            : GetCollection<T>().InsertManyAsync(transaction.Handle, values);
    }

    public Task Update<T>(T value, ITransaction? transaction = null) where T : IEntity
    {
        var filter = new FilterDefinitionBuilder<T>().Eq(t => t.Id, value.Id);
        var options = new ReplaceOptions { IsUpsert = false };
        return transaction is null
            ? GetCollection<T>().ReplaceOneAsync(filter, value, options)
            : GetCollection<T>().ReplaceOneAsync(transaction.Handle, filter, value, options);
    }

    private IMongoCollection<T> GetCollection<T>() => db.GetCollection<T>(CollectionName(typeof(T)));
    
    /// <summary>
    /// If the type contains a static string field named CollectionName, that will be used
    /// as the collection name in the database. Otherwise it's the name of the type.
    /// </summary>
    /// <param name="t"></param>
    /// <returns>Name of the collection to use in the actual database</returns>
    private static string CollectionName(Type t)
    {
        var field = t.GetField("CollectionName");
        var overridenValue = field?.GetValue(null);
        if (overridenValue is not null && overridenValue is string v)
            return v;
        return t.Name;
    }
}
