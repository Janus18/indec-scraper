using IndecScraper.Values;
using MongoDB.Bson.Serialization;

namespace IndecScraper.Infrastructure.Serializers;

public class PercentageBsonSerializer : IBsonSerializer<Percentage>
{
    public Type ValueType => typeof(Percentage);

    public Percentage Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        => new Percentage((decimal)context.Reader.ReadDecimal128());

    public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, Percentage value)
    {
        context.Writer.WriteDecimal128(new MongoDB.Bson.Decimal128(value.Value));
    }

    public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
    {
        if (value is Percentage p)
        {
            Serialize(context, args, p);
        }
    }

    object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        => Deserialize(context, args);
}

public class PercentageBsonSerializerProvider : IBsonSerializationProvider
{
    public IBsonSerializer? GetSerializer(Type type)
    {
        if (type == typeof(Percentage))
            return new PercentageBsonSerializer();
        return null;
    }
}