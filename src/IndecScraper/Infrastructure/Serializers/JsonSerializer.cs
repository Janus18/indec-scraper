using System.Text.Json;

namespace IndecScraper.Infrastructure.Serializers;

public static class JsonSerializer
{
    public static readonly JsonSerializerOptions Options = new JsonSerializerOptions
    {
        WriteIndented = true,
        Converters =
        {
            new PercentageJsonSerializer()
        }
    };

    public static string Serialize(object? x) => System.Text.Json.JsonSerializer.Serialize(x, Options);
}
