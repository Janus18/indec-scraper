using System.Text.Json;
using System.Text.Json.Serialization;
using IndecScraper.Values;

namespace IndecScraper.Infrastructure.Serializers;

public class PercentageJsonSerializer : JsonConverter<Percentage>
{
    public override Percentage? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        => new Percentage(reader.GetDecimal());

    public override void Write(Utf8JsonWriter writer, Percentage value, JsonSerializerOptions options)
        => writer.WriteNumberValue(value.Value);
}
