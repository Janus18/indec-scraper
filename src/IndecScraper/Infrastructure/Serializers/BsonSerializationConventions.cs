using IndecScraper.Models;
using IndecScraper.Models.Interfaces;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

namespace IndecScraper.Infrastructure.Serializers;

public static class BsonSerializationConventions
{
    public static void Register()
    {
        var pack = new ConventionPack();
        pack.Add(new StringIdStoredAsObjectIdConvention());
        ConventionRegistry.Register("StringIdConvention", pack, t => t.GetInterface(nameof(IEntity)) != null);

        BsonClassMap.RegisterClassMap<WebsiteCheckpoint>(cm =>
        {
            cm.AutoMap();
            cm.UnmapProperty(c => c.IsNew);
        });

        BsonSerializer.RegisterSerializationProvider(new PercentageBsonSerializerProvider());
    }
}
