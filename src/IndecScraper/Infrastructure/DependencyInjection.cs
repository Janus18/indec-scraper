using Amazon.Scheduler;
using IndecScraper.Application.Configuration;
using IndecScraper.Application.Services;
using IndecScraper.Infrastructure.Configuration;
using IndecScraper.Infrastructure.Implementation;
using IndecScraper.Infrastructure.Interfaces;
using IndecScraper.Infrastructure.Serializers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace IndecScraper.Infrastructure;

public static class DependencyInjection
{
    static DependencyInjection()
    {
        BsonSerializationConventions.Register();
    }

    public static IServiceCollection Inject(IServiceCollection services)
    {
        var configProvider = Configurations.GetConfigurationProvider();
        var config = configProvider.GetConfiguration(new ConfigurationBuilder());
        services.Configure<ApplicationConfiguration>(config);
        services.AddAWSService<IAmazonScheduler>();
        services.AddScoped<IDatabaseService, DatabaseService>();
        services.AddScoped<IWebReader, WebReader>();
        services.AddScoped<ILogger>((sp) => Logger.Instance);
        services.AddSingleton<IBuildConfiguration, BuildConfiguration>();
        return services;
    }
}
