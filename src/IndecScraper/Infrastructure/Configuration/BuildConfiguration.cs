using System.Reflection;
using IndecScraper.Application.Services;

namespace IndecScraper.Infrastructure.Configuration;

public class BuildConfiguration : IBuildConfiguration
{
    public string? Name()
        => typeof(BuildConfiguration).Assembly.GetCustomAttribute<AssemblyConfigurationAttribute>()?.Configuration;
}
