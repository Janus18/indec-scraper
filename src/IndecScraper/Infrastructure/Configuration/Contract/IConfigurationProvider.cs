using Microsoft.Extensions.Configuration;

namespace IndecScraper.Infrastructure.Configuration.Contract;

public interface IConfigurationProvider
{
    IConfiguration GetConfiguration(ConfigurationBuilder builder);
}