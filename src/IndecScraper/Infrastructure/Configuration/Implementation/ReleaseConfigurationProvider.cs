using IndecScraper.Providers;
using Microsoft.Extensions.Configuration;
using IConfigurationProvider = IndecScraper.Infrastructure.Configuration.Contract.IConfigurationProvider;

namespace IndecScraper.Infrastructure.Configuration.Implementation;

public class ReleaseConfigurationProvider : IConfigurationProvider
{
    public IConfiguration GetConfiguration(ConfigurationBuilder builder)
    {
        return builder
            .AddAWSSecretsManager("IndecScraper", "us-east-1")
            .Build();
    }
}