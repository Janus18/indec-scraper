using IndecScraper.Application.Configuration;
using Microsoft.Extensions.Configuration;
using IConfigurationProvider = IndecScraper.Infrastructure.Configuration.Contract.IConfigurationProvider;

namespace IndecScraper.Infrastructure.Configuration.Implementation;

public class DebugConfigurationProvider : IConfigurationProvider
{
    public IConfiguration GetConfiguration(ConfigurationBuilder builder)
    {
        return builder
            .AddJsonFile("appsettings.json", true)
            .AddUserSecrets<ApplicationConfiguration>()
            .Build();
    }
}