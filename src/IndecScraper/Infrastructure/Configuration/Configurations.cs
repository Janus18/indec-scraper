using IndecScraper.Infrastructure.Configuration.Implementation;
using IConfigurationProvider = IndecScraper.Infrastructure.Configuration.Contract.IConfigurationProvider;

namespace IndecScraper.Infrastructure.Configuration;

public static class Configurations
{
    private const string Debug = "Debug";
    private const string Release = "Release";

    public static IConfigurationProvider GetConfigurationProvider()
    {
        var buildConfig = new BuildConfiguration().Name();
        return buildConfig switch
        {
            Debug => new DebugConfigurationProvider(),
            Release => new ReleaseConfigurationProvider(),
            _ => throw new ArgumentException($"Unknown configuration name {buildConfig}")
        };
    }
}
