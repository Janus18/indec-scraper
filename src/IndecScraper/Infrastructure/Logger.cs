using Serilog;
using Serilog.Formatting.Json;

namespace IndecScraper.Infrastructure;

public static class Logger
{
    public static readonly ILogger Instance = new LoggerConfiguration()
        .WriteTo.Console(formatter: new JsonFormatter())
        .CreateLogger();
}