using HtmlAgilityPack;
using IndecScraper.Models.Interfaces;

namespace IndecScraper.Infrastructure.Interfaces;

public interface IWebReader
{
    Task<HtmlDocument> Read();

    Task<IWebsite> ReadWebsite();
}