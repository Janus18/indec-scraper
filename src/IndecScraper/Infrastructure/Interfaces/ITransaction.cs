using MongoDB.Driver;

namespace IndecScraper.Infrastructure.Interfaces;

public interface ITransaction
{
    IClientSessionHandle Handle { get; }
    Task Commit();
    Task Rollback();
}