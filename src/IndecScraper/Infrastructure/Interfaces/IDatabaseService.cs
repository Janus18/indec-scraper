using System.Linq.Expressions;
using IndecScraper.Models.Interfaces;
using IndecScraper.Infrastructure.Implementation;

namespace IndecScraper.Infrastructure.Interfaces;

public interface IDatabaseService
{
    Task<T?> GetFirst<T>(Expression<Func<T, bool>>? filter = null, DatabaseOrder<T>? orderBy = null, ITransaction? transaction = null)  where T : IEntity;
    Task<IEnumerable<T>> GetMany<T>(Expression<Func<T, bool>> filter, ITransaction? transaction = null) where T : IEntity;
    Task Save<T>(T value, ITransaction? transaction = null) where T : IEntity;
    Task SaveMany<T>(IEnumerable<T> values, ITransaction? transaction = null) where T : IEntity;
    Task Update<T>(T value, ITransaction? transaction = null) where T : IEntity;
    Task<ITransaction> CreateTransaction();
}
