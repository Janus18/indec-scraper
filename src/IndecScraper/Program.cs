using IndecScraper.Infrastructure.Serializers;

namespace IndecScraper;

public static class Program
{
    public static async Task Main(string[] args)
    {
        try
        {
            var command = Application.Command.GetCommand(args);
            var mediator = EntryPoint.GetMediator();
            var result = await mediator.Send(command);
            Console.WriteLine(JsonSerializer.Serialize(result));
        }
        catch (UnauthorizedAccessException)
        {
            Console.WriteLine("Unauthorized");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unknown exception");
            Console.WriteLine(ex.ToString());
        }
    }
}
