"use client";
import { useContext, useEffect, useState } from 'react';
import { ApiServiceContext } from '../services/apiService/ApiServiceContext';
import { MonthCPI } from '../models/MonthCPI';
import { DateTime } from 'luxon';
import { Error } from '@/components/Error';
import { SearchForm } from '@/components/Home/SearchForm';
import { Period } from '@/models/Period';
import CPIData from '@/components/Home/CPIData';
import Footer from '@/components/Home/Footer';
import { WebsiteCheckpoint } from '@/models/WebsiteCheckpoint';

export default function Home() {
  const apiService = useContext(ApiServiceContext);
  const [displayError, setDisplayError] = useState(false);
  const [data, setData] = useState<Array<MonthCPI>>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [lastUpdate, setLastUpdate] = useState<WebsiteCheckpoint | undefined>(undefined);

  const formInitialState = {
    from: DateTime.utc().startOf('year'),
    to: DateTime.utc().startOf('month')
  };
  
  const getData = (period: Period) => {
    if (period.from === undefined || period.to === undefined)
      return;
    setIsLoading(true);
    apiService.GetRange(period.from, period.to)
      .then(result => setData(result))
      .catch(() => setDisplayError(true))
      .finally(() => setIsLoading(false));
  }

  useEffect(() => getData(formInitialState), []);
  useEffect(() => {
    apiService.GetCheckpoint()
      .then(result => setLastUpdate(result))
      .catch(() => { });
  }, []);

  return (
    <>
      <main className="flex flex-col items-center min-h-screen">
        <div className="border-b border-secondary flex flex-row from-inherit items-center justify-between lg:px-20 mb-6 md:px-10 px-6 py-4 w-full">
          <div>
            <p>Argentina</p>
            <p className='font-semibold text-lg'>Consumer Price Index</p>
          </div>
          <div>
            Last updated on: {lastUpdate?.timestamp.toLocal().toFormat('yyyy-MM-dd HH:mm') ?? 'Never'}
          </div>
        </div>
        <div className='flex flex-col grow items-center lg:flex-row lg:items-start lg:justify-center px-8 w-full'>
          <SearchForm
            className='border border-secondary box-content flex flex-col items-center max-w-2xl mb-4 lg:w-48 p-4 rounded-md w-full'
            initialState={formInitialState}
            loading={isLoading}
            search={(p) => getData(p)} />
          <CPIData className='border border-secondary box-content lg:ml-6 max-w-2xl p-4 rounded-md w-full' loading={isLoading} values={data} />
        </div>
        <Footer />
      </main>
      {
        displayError &&
        <Error description='Could not get information from the server, try again later.' onClose={() => setDisplayError(false)} />
      }
    </>
  );
}
