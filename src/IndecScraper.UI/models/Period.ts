import { DateTime } from "luxon"

export type Period = {
    from: DateTime | undefined,
    to: DateTime | undefined
};
