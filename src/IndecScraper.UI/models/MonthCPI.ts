import { Round } from '@/shared/Number';
import { DateTime } from 'luxon';

export class MonthCPI {
    constructor(
        public datePublished: DateTime,
        public period: DateTime,
        public index: number,
        public accumulated: number
    ) {}

    public static Create(value: {
        DatePublished: string;
        Period: string;
        Index: number;
        Accumulated: number;
    }): MonthCPI {
        return new MonthCPI(
            DateTime.fromISO(value.DatePublished).toUTC(),
            DateTime.fromISO(value.Period).toUTC(),
            Round(value.Index, 2),
            Round(value.Accumulated, 2)
        );
    }
}
