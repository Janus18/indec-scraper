import { DateTime } from "luxon";

export class WebsiteCheckpoint {
    constructor(
        public timestamp: DateTime,
        public created: number,
        public updated: number,
    ) { }
    
    public static Create(value: {
        Timestamp: string;
        Created: number;
        Updated: number;
    }) {
        return new WebsiteCheckpoint(
            DateTime.fromISO(value.Timestamp).toUTC(),
            value.Created,
            value.Updated
        );
    }
}
