export function IsProdEnv() {
    return process.env.ENV_NAME === 'PROD';
}