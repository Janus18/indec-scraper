export function Round(x: number, digits: number): number {
    if (digits < 0)
        return Math.trunc(x);
    const multiple = Math.pow(10, Math.trunc(digits));
    return Math.round(x * multiple) / multiple;
}
