/** Create a list of numbers from zero to x */
export function Range(x: number): Array<number> {
    if (x < 0) {
        return [];
    }
    return Array.from(Array(x).keys());
}