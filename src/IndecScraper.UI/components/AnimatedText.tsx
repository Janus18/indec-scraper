import { useEffect, useState } from "react";

export type AnimatedTextProps = {
    values: Array<string>;
    animationTimeMs: number;
};

export default function AnimatedText(props: AnimatedTextProps) {
    const [currentIndex, setCurrentIndex] = useState(0);
    const valuesAmount = props.values.length;

    useEffect(() => {
        if (valuesAmount > 0) {
            setTimeout(() => setCurrentIndex(x => (x + 1) % valuesAmount), props.animationTimeMs);
        }
    });

    return (
        <>
            {valuesAmount > 0 && props.values[currentIndex]}
        </>
    );
}