import { DateTime } from "luxon";
import Button from "../Button";
import MonthPicker from "../MonthPicker";
import { useState } from "react";
import { Period } from "@/models/Period";

export type SearchFormProps = {
    initialState?: Period;
    search: (period: Period) => void;
    className?: string;
    loading?: boolean;
};

export function SearchForm(props: SearchFormProps) {
    const [formValue, setFormValue] = useState(
        props.initialState ?? { from: DateTime.utc(), to: DateTime.utc() }
    );
    return (
        <div className={props.className}>
            <div className='flex flex-col w-full'>
                <MonthPicker
                    className='w-full'
                    initialValue={formValue.from}
                    label='From'
                    onChange={(d) => setFormValue((v) => ({ ...v, from: d }))} />
                <MonthPicker
                    className='w-full'
                    initialValue={formValue.to}
                    label='To'
                    onChange={(d) => setFormValue((v) => ({ ...v, to: d }))} />
            </div>
            <Button className='w-full w-48' disabled={props.loading} onClick={() => props.search(formValue)}>
                Search
            </Button>
        </div>
    );
}