import Image from "next/image";
import gitlabImg from '../../public/gitlab.png';

export default function Footer() {
    return (
        <div className='border-t border-secondary bottom-0 flex flex-col items-center justify-between p-6 mt-6 md:flex-row static w-full'>
            <p className='max-w-lg mb-4 md:mb-0 md:text-left text-xs text-center'>
                Data extracted from the Argentina&apos;s INDEC website. This is a sample project and is not meant to be used as a reference
                for the data shown here, instead refer directly to the INDEC official website.
            </p>
            <a className='flex flex-col items-center' href='https://gitlab.com/Janus18/indec-scraper/-/tree/dev'>
                <span className='text-orange text-xs'>Source code</span>
                <Image alt='Gitlab icon' height={32} src={gitlabImg} width={32} />
            </a>
        </div>
    );
}