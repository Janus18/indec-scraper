import { MonthCPI } from '@/models/MonthCPI';
import dynamic from 'next/dynamic';
import { useState } from 'react';
import Select from '../Select';
import AnimatedText from '../AnimatedText';
import { Range } from '@/shared/Array';

const DynamicChart = dynamic(() => import('./CPIChart'), { ssr: false });
const DynamicTable = dynamic(() => import('./CPITable'), { ssr: false });

export type CPIDataProps = {
    values: Array<MonthCPI>;
    className?: string;
    loading?: boolean;
};

type DisplayType = 'CHART' | 'TABLE';

export default function CPIData(props: CPIDataProps) {
    const [displayType, setDisplayType] = useState<DisplayType>('TABLE');
    const displayTypes: Array<{ description: string, value: DisplayType }> = [
        { description: 'Chart', value: 'CHART' },
        { description: 'Table', value: 'TABLE' }
    ];
    const lastValue = props.values.findLast(() => true)?.accumulated ?? 0;
    return (
        <div className={`flex flex-col ${props.className}`}>
            {
                props.loading &&
                <>
                    <p className='text-center w-full'>
                        Waiting for data
                    </p>
                    <p className='text-center w-full'>
                        <AnimatedText animationTimeMs={500} values={Range(30).map(x => ''.padStart(x + 1, '.'))} />
                    </p>
                </>
            }
            {
                !props.loading &&
                <div className='flex flex-row justify-between mb-4'>
                    <span className='font-semibold'>Accumulated CPI: {lastValue.toFixed(2)}</span>
                    <Select value={displayType} onChange={(x) => setDisplayType(x as DisplayType)} options={displayTypes} />
                </div>
            }
            {!props.loading && displayType === 'CHART' && <DynamicChart values={props.values} />}
            {!props.loading && displayType === 'TABLE' && <DynamicTable values={props.values} />}
        </div>
    );
}