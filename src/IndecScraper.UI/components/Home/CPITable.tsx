import { MonthCPI } from "@/models/MonthCPI";

export type CPITableProps = {
    values: Array<MonthCPI>;
};

export default function CPITable(props: CPITableProps) {
    const headerCellClassName = 'pb-2 hidden sm:table-cell';
    const bodyCellClassName = 'py-2 hidden sm:table-cell';

    const rows = props.values.map(v => (
        <tr className='border-b border-b-gray' key={v.period.toISODate()}>
            <td className={`${bodyCellClassName} text-start`}>{v.period.toFormat('yyyy-MM')}</td>
            <td className={`${bodyCellClassName} text-end`}>{v.index.toString(10)}</td>
            <td className={`${bodyCellClassName} text-end`}>{v.accumulated.toString(10)}</td>
            <td className='p-2 sm:hidden'>
                <p>Month: {v.period.toFormat('yyyy-MM')}</p>
                <p>Index: {v.index.toString(10)}</p>
                <p>Accumulated: {v.accumulated.toString(10)}</p>
            </td>
        </tr>
    ));

    return (
        <table className='w-full'>
            <thead>
                <tr className='border-b border-b-gray'>
                    <th className={`${headerCellClassName} text-start`}>Month</th>
                    <th className={`${headerCellClassName} text-end`}>Index</th>
                    <th className={`${headerCellClassName} text-end`}>Accumulated</th>
                    <th className='sm:hidden'>CPI Values Found</th>
                </tr>
            </thead>
            <tbody>
                {rows}
            </tbody>
        </table>
    );
}
