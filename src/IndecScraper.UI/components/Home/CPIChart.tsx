import Chart from "../Chart";
import { MonthCPI } from "@/models/MonthCPI";

export type CPIChartProps = {
    values: Array<MonthCPI>;
};

export default function CPIChart(props: CPIChartProps) {
  const chartData = {
    options: {
      chart: {
        id: 'cpi-history',
        foreColor: 'white',
        toolbar: {
          tools: {
            download: false
          }
        }
      },
      fill: {
        colors: ['#c4b5fd']
      },
      tooltip: {
        theme: 'dark',
      },
      xaxis: {
          categories: props.values.map(x => x.period.toUTC().toFormat('yyyy-LL')),
          tickAmount: 10
      }
    },
    series: [
      {
        name: 'CPI',
        data: props.values.map(x => x.index),
        color: '#90CAF9'
      },
      {
        name: 'Accumulated CPI',
        data: props.values.map(x => x.accumulated),
        color: '#FAC091'
      }
    ]
  };

  return (
    <Chart options={chartData.options} series={chartData.series} type="line" />
  )
}