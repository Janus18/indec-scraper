import { DateTime } from "luxon";
import { useState } from "react";
import Input from "./Input";

const monthValidator = /^(0?[1-9]|10|11|12)$/;
const yearValidator = /^\d\d\d\d$/;

type MonthPickerProps = {
    onChange: (x: DateTime | undefined) => void,
    initialValue?: DateTime,
    className?: string,
    label?: string,
};

export default function MonthPicker(props: MonthPickerProps) {
    const [isInvalidMonth, setIsInvalidMonth] = useState(false);
    const [isInvalidYear, setIsInvalidYear] = useState(false);
    const [monthValue, setMonthValue] = useState(props.initialValue?.month.toString(10) ?? '');
    const [yearValue, setYearValue] = useState(props.initialValue?.year.toString(10) ?? '');
    
    const setNextValue = (year: string, month: string) => {
        const nextValue = !yearValidator.test(year) || !monthValidator.test(month)
            ? undefined
            : DateTime.fromISO(`${year}-${month.padStart(2, '0')}-01T00:00:00.000`).toUTC();
        props.onChange(nextValue);
    }

    const onMonthChange = (el: HTMLInputElement) => {
        if (el.value.length > 2)
            return;
        setMonthValue(el.value);
        setIsInvalidMonth(!monthValidator.test(el.value));
        setNextValue(yearValue, el.value);
    };

    const onYearChange = (el: HTMLInputElement) => {
        if (el.value.length > 4)
            return;
        setYearValue(el.value);
        setIsInvalidYear(!yearValidator.test(el.value));
        setNextValue(el.value, monthValue);
    };

    return (
        <div className={`flex flex-col items-center ${props.className}`}>
            <div>
                <span className='w-10 block mr-2 '>
                    {props.label}
                </span>
                <Input
                    className='w-12'
                    invalid={isInvalidMonth}
                    onChange={(event) => onMonthChange(event.target)}
                    placeholder='mm'
                    type='text'
                    value={monthValue} />
                <span className='text-white text-xl mx-2'>/</span>
                <Input
                    className='w-20'
                    invalid={isInvalidYear}
                    onChange={(event) => onYearChange(event.target)}
                    placeholder='yyyy'
                    type='text'
                    value={yearValue} />
            </div>
            <span className='text-center text-red h-6 mt-2'>
                {isInvalidMonth || isInvalidYear ? 'Invalid' : ''}
            </span>
        </div>
    );
}
