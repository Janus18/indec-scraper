export type SelectProps = {
    className?: string;
    value?: string | number;
    onChange: (x: string | number) => void;
    options: Array<{ description: string, value: string | number }>;
};

export default function Select(props: SelectProps) {
    return (
        <select className={`
            border-2
            border-secondary
            bg-contrast
            focus:border-2-white
            focus:outline-none
            h-7
            px-6
            placeholder:text-zinc-300
            rounded
            text-white
            text-center
            ${props.className}
        `}
            onChange={(event) => props.onChange(event.target.value)}
            value={props.value}>
            {props.options.map(x => <option key={x.value} value={x.value}>{x.description}</option>)}
        </select>
    )
}