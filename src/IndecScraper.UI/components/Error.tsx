import Button from "./Button";

export type ErrorProps = {
    description: string,
    onClose: () => void,
};

export function Error(props: ErrorProps) {
    return (
        <div className="
            bg-opacity-70
            bg-primary
            fixed
            flex
            flex-row
            h-full
            items-center
            justify-center
            left-0
            top-0
            w-full">
            <div className={`
                bg-red
                flex
                flex-col
                py-3
                px-4
                rounded-xl
                w-64`}>
                <h3 className='mb-2 text-center w-full'>Error</h3>
                <p className='text-sm'>{props.description}</p>
                <Button
                    backgroundColor='bg-red'
                    className='border border-white mt-4'
                    onClick={() => props.onClose()}
                    textColor='white'>
                    Close
                </Button>
            </div>
        </div>
    );
}