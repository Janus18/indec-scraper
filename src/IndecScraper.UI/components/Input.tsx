import { DetailedHTMLProps, InputHTMLAttributes } from "react";

export type InputProps =
    DetailedHTMLProps<InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> &
    { invalid?: boolean };

export default function Input(props: InputProps) {
    const { className, ...noClassNameProps } = props;
    return <input className={`
        border-2
        ${props.invalid === true ? 'border-red' : 'border-secondary'}
        bg-contrast
        ${props.invalid === true ? '' : 'focus:border-2-white'}
        focus:outline-none
        h-7
        px-1
        placeholder:text-zinc-300
        rounded
        text-white
        text-center
        ${className}
    `} {...noClassNameProps} />
}