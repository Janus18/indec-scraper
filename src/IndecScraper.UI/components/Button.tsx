import { ButtonHTMLAttributes, DetailedHTMLProps } from "react";

type ButtonProps = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> &
{
    backgroundColor?: string;
    textColor?: string;
};

export default function Button(props: ButtonProps) {
    return (
        <button {...props} className={`
            ${props.backgroundColor ?? 'bg-secondary'}
            disabled:bg-gray
            focus:border
            focus:border-white
            focus:outline-none
            h-7
            px-4
            rounded
            ${props.textColor ?? 'text-primary'}
            text-center
            ${props.className}`}>
        </button>
    )
}
