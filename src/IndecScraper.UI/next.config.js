
require('dotenv').config();

/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    API_BASE_URL: process.env.API_BASE_URL,
    ENV_NAME: process.env.ENV_NAME
  },
  experimental: {
    appDir: true,
  },
  images: {
    unoptimized: true
  },
  output: 'export',
};

module.exports = nextConfig
