import { createContext } from "react";
import { IApiService } from "./IApiService";
import { ApiService } from "./ApiService";
import { IsProdEnv } from "@/shared/Environment";
import { FakeApiService } from "./FakeApiService";

export const ApiServiceContext = createContext<IApiService>(
    IsProdEnv()
        ? new ApiService(process.env.API_BASE_URL!)
        : new FakeApiService()
);