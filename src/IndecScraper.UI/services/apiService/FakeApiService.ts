import { MonthCPI } from "@/models/MonthCPI";
import { DateTime } from "luxon";
import { IApiService } from "./IApiService";
import { faker } from '@faker-js/faker';
import { Range } from "@/shared/Array";
import { WebsiteCheckpoint } from "@/models/WebsiteCheckpoint";

export class FakeApiService implements IApiService {
    async GetRange(from: DateTime, to: DateTime): Promise<MonthCPI[]> {
        if (from > to) {
            return [];
        }
        const months = Math.ceil(to.diff(from, ['months']).months) + 1;
        const values = Range(months)
            .map(x => from.plus({ months: x }))
            .map(d => new MonthCPI(d, d, faker.datatype.number(), faker.datatype.number()));
        const promise = new Promise<Array<MonthCPI>>((resolve) => {
            setTimeout(() => resolve(values), faker.datatype.number({ min: 1000, max: 10000 }));
        });
        return await promise;
    }

    async GetCheckpoint(): Promise<WebsiteCheckpoint | undefined> {
        const checkpoint = new WebsiteCheckpoint(
            DateTime.utc(),
            faker.datatype.number({ min: 0, max: 100 }),
            faker.datatype.number({ min: 0, max: 100 })
        );
        const promise = new Promise<WebsiteCheckpoint>((resolve) => {
            setTimeout(() => resolve(checkpoint), faker.datatype.number({ min: 1000, max: 10000 }));
        });
        return await promise;
    }
}