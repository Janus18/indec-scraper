import { DateTime } from 'luxon';
import { MonthCPI } from '../../models/MonthCPI';
import { WebsiteCheckpoint } from '@/models/WebsiteCheckpoint';

export interface IApiService {
    GetRange(from: DateTime, to: DateTime): Promise<Array<MonthCPI>>;
    GetCheckpoint(): Promise<WebsiteCheckpoint | undefined>;
}
