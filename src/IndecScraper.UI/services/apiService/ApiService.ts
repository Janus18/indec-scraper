import { WebsiteCheckpoint } from "@/models/WebsiteCheckpoint";
import { MonthCPI } from "../../models/MonthCPI";
import { PostJson } from "../../shared/FetchExtensions";
import { IApiService } from "./IApiService";
import { DateTime } from "luxon";

export class ApiService implements IApiService {
    constructor(private apiBaseUrl: string) {}

    public async GetRange(from: DateTime, to: DateTime): Promise<Array<MonthCPI>> {
        const command = ['Values', from.toFormat('yyyy-MM'), to.toFormat('yyyy-MM')];
        const response = await PostJson(this.apiBaseUrl, command);
        if (!response.ok) {
            throw { status: response.status, error: 'GetRange status is not ok' };
        }
        const array = await response.json();
        return array.map((x: object) => MonthCPI.Create(x as any));
    }

    public async GetCheckpoint(): Promise<WebsiteCheckpoint | undefined> {
        var response = await PostJson(this.apiBaseUrl, ['Checkpoint']);
        if (!response.ok) {
            throw { status: response.status, error: 'GetCheckpoint status is not ok' };
        }
        const body = await response.json();
        return body !== null && body !== undefined ? WebsiteCheckpoint.Create(body as any) : undefined;
    }
}
