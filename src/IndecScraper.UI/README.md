# INDEC Scraper Front-End

This is a user interface for a better visualization of the data extracted and kept by
the sibling back-end project. This web consumes the interface offered by that project
through HTTP (i.e., the back-end project is uploaded as a Lambda, but then exposed as an HTTP API).

The web contains a simple search form and a data visualization component. Data can be displayed in either
a table or a chart, which is not too well suited for mobile devices but the options is there. The total
accumulated CPI for the range selected is also shown there, as well as the last time the synchronization
took place.

This project was built using Next.js, configured with TypeScript and Tailwind.
Charts were made with [apexcharts](https://github.com/apexcharts/react-apexcharts).

### Getting Started - Debug

NPM was used as the package manager (v9.2). First install all dependencies (`npm install`),
then start the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Don't worry about the back-end yet, the dev script uses the DEV environment, which in turn
will indicate the app to replace the real `ApiService` with a `FakeApiService`, the data is mocked
using the [faker library](https://fakerjs.dev/). These services can be found in the services/apiService
folder, and are provided with a React Context.

### Deploy
Before the deploy you'll need to tell the app where to make its requests to. As you can see in
`next.config.js`, that URL must be passed as an environment variable with the name `API_BASE_URL`.

You can either use a `.env` file (the project uses the [dotenv library](https://www.dotenv.org/docs/)
to set up environment variables) or have the env variable defined in your session when you compile
the project.

You'll see there's another environment variable `ENV_NAME` in next.config.js, but that is already
taken care of in the `package.json` scripts.

With `API_BASE_URL` defined you can now go on and build the project

```bash
npm run build
```

The Next config states `output: 'export'`, so the result is a SPA website in the `out` directory.
You'll need a local (e.g., http-server) or cloud HTTP server (e.g., upload the files to S3 and
turn on the website settings) to run it.
