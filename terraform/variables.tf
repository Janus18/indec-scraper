variable "aws_region" {
  type        = string
  description = "Region for AWS Resources"
  default     = "us-east-1"
}

variable "lambda_name" {
  type        = string
  description = "Name for Lambda function"
  default     = "indecscraper"
}

variable "s3_bucket_name" {
  type        = string
  description = "Name of the S3 bucket used to store the frontend code"
}
