output "eventbridge_role_arn" {
  value = aws_iam_role.iam_for_eventbridge.arn
}

output "lambda_arn" {
  value = aws_lambda_function.indecscraper_lambda.arn
}

output "apigateway_url" {
  value = aws_apigatewayv2_stage.defaultstage.invoke_url
}
