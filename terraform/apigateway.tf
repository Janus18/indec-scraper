resource "aws_apigatewayv2_api" "apigateway" {
  cors_configuration {
    allow_headers = ["*"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }
  name          = "indecscraper"
  protocol_type = "HTTP"
  tags          = local.common_tags
}

resource "aws_apigatewayv2_integration" "apigateway_lambda_integration" {
  api_id           = aws_apigatewayv2_api.apigateway.id
  integration_type = "AWS_PROXY"

  connection_type      = "INTERNET"
  description          = "Integration to lambda"
  integration_method   = "POST"
  integration_uri      = aws_lambda_function.indecscraper_lambda.invoke_arn
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route" "apigateway_lambda_route" {
  api_id    = aws_apigatewayv2_api.apigateway.id
  route_key = "POST /Indecscraper"
  target    = "integrations/${aws_apigatewayv2_integration.apigateway_lambda_integration.id}"
}

resource "aws_apigatewayv2_stage" "defaultstage" {
  api_id      = aws_apigatewayv2_api.apigateway.id
  name        = "default"
  auto_deploy = true
  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.indecscraper.arn
    format          = "{ \"requestId\":\"$context.requestId\", \"extendedRequestId\":\"$context.extendedRequestId\", \"ip\": \"$context.identity.sourceIp\", \"caller\":\"$context.identity.caller\", \"user\":\"$context.identity.user\", \"requestTime\":\"$context.requestTime\", \"httpMethod\":\"$context.httpMethod\", \"resourcePath\":\"$context.resourcePath\", \"status\":\"$context.status\", \"protocol\":\"$context.protocol\", \"responseLength\":\"$context.responseLength\" }"
  }
  tags = local.common_tags
}
