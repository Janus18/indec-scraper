resource "aws_cloudwatch_log_group" "indecscraper" {
  name              = "/aws/lambda/${var.lambda_name}"
  retention_in_days = 7
  tags = local.common_tags
  lifecycle {
    prevent_destroy = false
  }
}
