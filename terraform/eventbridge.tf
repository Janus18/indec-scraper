data "aws_iam_policy_document" "eventbridge_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["scheduler.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "invoke_lambda_policy" {
  statement {
    effect  = "Allow"
    actions = ["lambda:InvokeFunction"]
    resources = [
      aws_lambda_function.indecscraper_lambda.arn,
      "${aws_lambda_function.indecscraper_lambda.arn}:*"
    ]
  }
}

resource "aws_iam_role" "iam_for_eventbridge" {
  assume_role_policy = data.aws_iam_policy_document.eventbridge_assume_role.json
  inline_policy {
    name   = "EventBridge-Scheduler-Execution-Policy"
    policy = data.aws_iam_policy_document.invoke_lambda_policy.json
  }
  name = "indecscraper_eventbridge_role"
  tags = local.common_tags
}
