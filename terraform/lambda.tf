data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "iam_for_lambda" {
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/AWSLambdaExecute",
    "arn:aws:iam::aws:policy/SecretsManagerReadWrite",
    "arn:aws:iam::aws:policy/AmazonEventBridgeSchedulerFullAccess"
  ]
  name = "indecscraper_lambda_role"
  tags = local.common_tags
}

resource "aws_lambda_function" "indecscraper_lambda" {
  function_name = var.lambda_name
  role          = aws_iam_role.iam_for_lambda.arn
  tags          = local.common_tags
  timeout       = 300
  memory_size   = 256
  image_uri     = "${aws_ecr_repository.repository.repository_url}:latest"
  image_config {
    command = [
      "IndecScraper::IndecScraper.Function::FunctionHandler"
    ]
    entry_point = []
  }
  package_type = "Image"
  depends_on   = [aws_cloudwatch_log_group.indecscraper]
}

resource "aws_lambda_permission" "apigw_lambda" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.indecscraper_lambda.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.apigateway.execution_arn}/*"
}
