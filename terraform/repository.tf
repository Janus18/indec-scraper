data "aws_ecr_authorization_token" "token" {}

resource "aws_ecr_repository" "repository" {
  name         = "indecscraper"
  tags         = local.common_tags
  force_delete = true

  provisioner "local-exec" {
    # Put a dummy image into the repo, because the lambda function needs to be created with one.
    command    = <<EOF
      docker login ${data.aws_ecr_authorization_token.token.proxy_endpoint} -u AWS -p ${data.aws_ecr_authorization_token.token.password}
      docker pull alpine
      docker tag alpine ${aws_ecr_repository.repository.repository_url}:latest
      docker push ${aws_ecr_repository.repository.repository_url}:latest
      EOF
    on_failure = fail
  }
}

resource "aws_ecr_lifecycle_policy" "delete-untagged" {
  repository = aws_ecr_repository.repository.name
  policy     = <<EOF
    {
        "rules": [
            {
                "rulePriority": 1,
                "description": "Expire images older than 7 days",
                "selection": {
                    "tagStatus": "untagged",
                    "countType": "sinceImagePushed",
                    "countUnit": "days",
                    "countNumber": 7
                },
                "action": {
                    "type": "expire"
                }
            }
        ]
    }
    EOF
}
