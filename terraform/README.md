# INDEC Scraper Infrastructure

This directory contains the terraform description of the AWS infrastructure needed
to run both projects.

### Images repository

We'll need a Docker images repository to store the generated back-end images. An
ECR is created in `repository.tf` to do it.

The Lambda function created next must point to a valid image, so we cannot leave
this repo empty. That's why a dummy image is initially loaded.
Each new version uploaded must be tagged as `latest` to be used by the Lambda. All
untagged images will be deleted after seven days, so you don't need to worry about
a lot of stale images. 

### Lambda

As you can see in the back-end project, it is prepared to be run as an AWS Lambda
function. The `lambda.tf` file contains that definition as well as the role which
will execute the function.

Keep in mind that the timeout and memory size values are also detailed in the
back-end's `aws-lambda-tools-defaults.json` file, so if the values are not in sync,
each time you change the terraform files and plan, those values will change again if
you made a deploy of the back-end. The function name must also be respected. Using
the AWS dotnet cli tools with a different name will create another Lambda.

### Api Gateway

The Lambda is exposed to the public via Api Gateway as an HTTP endpoint. You can
find the Api Gateway definition in `apigateway.tf` with the required stage and route too.
CORS is also configured so we can make requests from our front-end.

### CloudWatch

We may need to debug problems in the deployment. It is easy to keep Lambda logs in CloudWatch,
we just need the Log Group created in `log-group.tf`. Logs are keeped for 7 days. The
Api Gateway uses that same Log Group to send logs to.

### S3

There's the `s3.tf` file which creates a S3 bucket used for the front-end build.
Be cautious as the bucket is publicly available and hosted as a website.

The code for the front-end should be uploaded manually after the bucket is deployed. Each
of the files should have a public-read ACL definition so it can be publicly accessed.
Here's a script you can execute in the IndecScraper.UI directory to make it simple:

```bash
npm run build
aws s3 rm --recursive s3://{Your_S3_bucket_name}/
aws s3 cp --recursive --acl "public-read" ./out/ s3://{Your_S3_bucket_name}/
```

As you can see, the name of the S3 bucket is not included so you can use your domain name.
If you do that, you can later point a DNS record to it, and have SSL enabled.
To provide the name, you can use any of the means offered by Terraform to define a variable,
like an argument in the CLI or a .tfvars file.

### EventBridge

The `eventbridge.tf` file contains the description of a role which will be used by
the EventBridge schedules created by the application to execute the synchronizations.
The role must be able to invoke the Lambda function.

### How to use
Just use the common Terraform CLI commands. You may want to validate first:

```bash
terraform validate
```

Then plan:

```bash
terraform plan -out 1.tfplan
```

And finally apply:

```bash
terraform apply 1.tfplan
```

Even if everything is created ok, nothing will quite work until you upload the code of both projects.
